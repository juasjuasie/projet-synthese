This is a demo of a 2-D unity game with the objective to evade the obstacles and shoot targets to reach the goal.

Game made by:

- Antony Ménard : https://gitlab.com/BlackMarcel1

- Gabriel St-Laurent-Recoura : https://gitlab.com/Gabnixe

- Nicolas Bisson : https://gitlab.com/Nicolas-Bisson

- Eduardo Yvan Breton Corona : https://gitlab.com/juasjuasie


These guys are pretty talented too, so please check them out if you are interested.
