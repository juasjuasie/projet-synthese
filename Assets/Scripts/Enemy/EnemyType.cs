﻿namespace Game
{
    //Author: Antony Ménard
    public enum EnemyType
    {
        Idle,
        Blaster,
        Missile,
        Laser
    }
}