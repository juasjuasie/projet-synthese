﻿using Game;
using UnityEngine;

namespace Game
{
    //Author: Gabriel St-laurent-Recoura
    public abstract class ShootingEnemy : EnemyController
    {
        [SerializeField] protected Direction shotDirection;
        [SerializeField] protected GameObject shotSpawnPoint;

        protected Vector3 shotSpawnPointPosition;
        protected Quaternion shotSpawnPointRotation;

        protected void Awake()
        {
            base.Awake();
            shotSpawnPoint.transform.Rotate(Vector3.forward, shotDirection.ToAngle());
            enemyAnimator.RotateEnemy(shotDirection);
            shotSpawnPointPosition = shotSpawnPoint.transform.position;
            shotSpawnPointRotation = shotSpawnPoint.transform.rotation;
        }

        protected virtual void Fire()
        {
            sfxHandler.PlayFireSfx();
        }
    }
}