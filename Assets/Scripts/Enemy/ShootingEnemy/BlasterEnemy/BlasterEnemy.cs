﻿using System.Collections;
using UnityEngine;

namespace Game
{
    //Author: Antony Ménard
    public class BlasterEnemy : ShootingEnemy
    {
        [SerializeField] private GameObject bulletToInstantiate;
        [SerializeField] private float timeBetweenShotsInSeconds = 0;

        private void OnEnable()
        {
            StartCoroutine(ShootBulletRoutine());
        }
        
        private IEnumerator ShootBulletRoutine()
        {
            while (isActiveAndEnabled)
            {
                yield return new WaitForSeconds(timeBetweenShotsInSeconds);
                Fire();
            }
        }

        protected override void Fire()
        {
            base.Fire();
            Instantiate(bulletToInstantiate, shotSpawnPointPosition, shotSpawnPointRotation);
        }
    }
}