﻿using UnityEngine;

namespace Game
{
    //Author: Antony Ménard, Gabriel St-Laurent-Recoura
    public class MissileEnemy : ShootingEnemy
    {
        [SerializeField] private GameObject missileToInstantiate;

        public void ShootMissile()
        {
            if (this != null)
                Fire();
        }

        protected override void Fire()
        {
            base.Fire();
            Instantiate(missileToInstantiate, shotSpawnPointPosition, shotSpawnPointRotation);
        }
    }
}