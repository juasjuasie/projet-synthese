﻿using UnityEngine;

namespace Game
{
    //Author: Antony Ménard
    public class LaserEnemy : ShootingEnemy
    {
        [SerializeField] private GameObject laserToInstantiate;
        
        private void Start()
        {
            Fire();
        }

        protected override void Fire()
        {
            sfxHandler.PlayFireSfx();
            sfxHandler.FireSource.loop = true;
            Instantiate(laserToInstantiate, shotSpawnPoint.transform);
        }
    }
}