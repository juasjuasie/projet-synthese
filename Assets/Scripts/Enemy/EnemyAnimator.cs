﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Game
{
    //Author: Gabriel St-laurent-Recoura
    public class EnemyAnimator : MonoBehaviour
    {
        [SerializeField] private GameObject enemy;
        [SerializeField] private GameObject feature;
        [SerializeField] private GameObject weapon;
        [SerializeField] private GameObject spikes;

        const int ANGLE_TO_ROTATE_FEATURE_IN_X_AND_Z = 180;

        private Vector3 vector3ToRotateTo;

        private void Awake()
        {
            vector3ToRotateTo = new Vector3(0,0,360);
        }
        

        public void RotateEnemy(Direction direction)
        {
            RotateWeapon(direction);
            RotateFeature(direction);
        }

        private void RotateWeapon(Direction direction)
        {
            weapon.transform.Rotate(Vector3.forward, direction.ToAngle());
        }

        private void RotateFeature(Direction direction)
        {
            if (direction == Direction.Right ||
                direction == Direction.DownRight ||
                direction == Direction.UpRight)
            {
                feature.transform.Rotate(Vector3.forward, ANGLE_TO_ROTATE_FEATURE_IN_X_AND_Z);
                feature.transform.Rotate(Vector3.right, ANGLE_TO_ROTATE_FEATURE_IN_X_AND_Z);
            }
        }

        public void RotateSpike()
        {
            spikes.transform.DORotate(vector3ToRotateTo, 1, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
        }

        private void OnDestroy()
        {
            spikes.transform.DOKill();
            enemy.transform.DOKill();
        }
    }
}