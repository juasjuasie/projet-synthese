﻿using System;
using Game;

namespace Game
{
    //Author: Gabriel St-Laurent-Recoura
    public class IdleEnemy : EnemyController
    {
        private void Start()
        {
            enemyAnimator.RotateSpike();
        }
    }
}