﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Antony Ménard, Gabriel St-Laurent-Recoura, Nicolas Bisson
    public abstract class EnemyController : MonoBehaviour, IShotable
    {
        [SerializeField] [Range(0, 10000)] private uint scoreValue;

        [SerializeField] protected EnemySfxHandler sfxHandler;
        [SerializeField][Range(0, 100)] protected float particleShowTime = 0f;
        
        private ParticleController particleController;
        private ISensor<PlayerController> sensorPlayer;
        private ISensor<PlayerLaser> sensorProjectile;
        private GameController gameController;
        protected EnemyAnimator enemyAnimator;
        
        protected void Awake()
        {
            particleController = GetComponentInChildren<ParticleController>();
            sensorPlayer = GetComponentInChildren<Sensor>().For<PlayerController>();
            gameController = Finder.GameController;
            enemyAnimator = GetComponentInChildren<EnemyAnimator>();
        }

        private void OnEnable()
        {
            sensorPlayer.OnSensedObject += OnPlayerSensed;
        }
        
        private void OnDisable()
        {
            sensorPlayer.OnSensedObject -= OnPlayerSensed;
        }
        
        private void OnPlayerSensed(PlayerController player)
        {
            player.Kill();
        }
        
        private void Kill()
        {
            particleController.ActivateParticles(true);
            particleController.ChangeParent(transform.parent, particleShowTime);
            sfxHandler.PlayDeathSfx();
            sfxHandler.transform.parent = transform.parent;
            gameController.AddScore(scoreValue);
            Destroy(gameObject);
        }

        public void OnShot()
        {
            StartCoroutine(WaitForEndOfFrameToKill());
        }

        private IEnumerator WaitForEndOfFrameToKill()
        {
            yield return new WaitForEndOfFrame();
            Kill();
        }
    }
}