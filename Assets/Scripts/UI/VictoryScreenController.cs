﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game
{
    //Author: Nicolas Bisson
    [Findable(R.S.Tag.Victory)]
    public class VictoryScreenController : MonoBehaviour
    {
        [SerializeField] private Text scoreText;

        private const string SCORE_TEXT_FORMAT = "Your score earned is: {0:00000000} ";

        private Canvas victoryCanvas;
        private GameController gameController;
        private GlobalController globalController;
        private GameHasEndedFormallyEventChannel gameHasEndedFormallyEventChannel;
        private PauseController pauseController;

        private void Awake()
        {
            pauseController = Finder.PauseController;
            victoryCanvas = GetComponent<Canvas>();
            victoryCanvas.enabled = false;
            gameController = Finder.GameController;
            globalController = Finder.GlobalController;
            gameHasEndedFormallyEventChannel = Finder.GameHasEndedFormallyEventChannel;
            enabled = false;
        }
        
        private void Update()
        {
            if (Input.anyKeyDown)
            {
                globalController.GoToMenu();
            }
        }

        public void Show()
        {
            victoryCanvas.enabled = true;
            gameHasEndedFormallyEventChannel.Publish();
            SetScoreOnCanvas();
        }

        private void SetScoreOnCanvas()
        {
            scoreText.text = string.Format(SCORE_TEXT_FORMAT, gameController.Score);
        }
    }
}