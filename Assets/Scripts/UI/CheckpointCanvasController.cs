﻿using Harmony;
using TMPro;
using UnityEngine;

namespace Game
{
    //Author: Antony Ménard
    [Findable(R.S.Tag.CheckpointCanvasController)]
    public class CheckpointCanvasController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI checkpointText;

        private void Awake()
        {
            ActivateCanvas(false);
        }

        public void ActivateCanvas(bool isTextNeeded)
        {
            checkpointText.enabled = isTextNeeded;
        }
    }
}