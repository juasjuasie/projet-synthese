﻿using System;
using System.Collections;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game
{
    //Author: Nicolas Bisson, Gabriel St-Laurent-Recoura
    [Findable(R.S.Tag.TimeToBeBold)]
    public class TimeToBeBoldController : MonoBehaviour
    {
        private Canvas timeToBeBoldCanvas;
        private TextMeshProUGUI timeToBeBoldText;

        private void Awake()
        {
            timeToBeBoldCanvas = GetComponent<Canvas>();
            timeToBeBoldText = GetComponentInChildren<TextMeshProUGUI>();
            timeToBeBoldCanvas.enabled = false;
        }

        public void EnableCanvas()
        {
            timeToBeBoldCanvas.enabled = true;
        }

        public void DisableCanvas()
        {
            timeToBeBoldCanvas.enabled = false;
        }

        public void ChangeTimeToBeBoldText(string newTimeToBeBoldText)
        {
            timeToBeBoldText.text = newTimeToBeBoldText;
        }
    }
}