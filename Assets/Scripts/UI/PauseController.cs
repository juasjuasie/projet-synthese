﻿using System;
using Harmony;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Cursor = UnityEngine.Cursor;

namespace Game
{
    //Author Eduardo Breton, Antony Ménard
    [Findable(R.S.Tag.PauseController)]
    public class PauseController : MonoBehaviour
    {
        [SerializeField] private KeyCode pauseKey = KeyCode.Escape;

        private Canvas pauseCanvas;
        private Button quitButton;
        private GlobalController globalController;
        private TimeScalerController timeScaler;
        private Slider slider;
        private MusicController musicController;
        private bool isGamePaused;
        private VolumeChangedEventChannel volumeChangedEventChannel;
        private bool canBePaused;
        private LoadingScreenController loadingScreenController;

        private void Awake()
        {
            volumeChangedEventChannel = Finder.VolumeChangedEventChannel;
            loadingScreenController = Finder.LoadingScreenController;
            canBePaused = false;
            isGamePaused = false;
            musicController = Finder.MusicController;
            slider = GetComponentInChildren<Slider>();
            timeScaler = Finder.TimeScalerController;
            pauseCanvas = GetComponent<Canvas>();
            quitButton = GetComponentInChildren<Button>();
            globalController = Finder.GlobalController;

            Cursor.visible = true;
            pauseCanvas.enabled = false;
            slider.value = musicController.GetGameVolume();
        }

        public void OnEnable()
        {
            slider.onValueChanged.AddListener(OnVolumeChanged);
            quitButton.onClick.AddListener(OnQuitButtonClicked);
        }

        public void OnDisable()
        {
            slider.onValueChanged.RemoveListener(OnVolumeChanged);
            quitButton.onClick.RemoveListener(OnQuitButtonClicked);
        }

        private void Update()
        {
            if (Input.GetKeyDown(pauseKey) && loadingScreenController.CanBePaused)
            {
                if (!isGamePaused)
                    Pause();
                else
                    Continue();
            }
        }

        private void OnVolumeChanged(float volume)
        {
            volumeChangedEventChannel.Publish(volume);
        }

        private void Pause()
        {
            isGamePaused = true;
            pauseCanvas.enabled = true;
            timeScaler.ActivateTimeScale(TimeScale.Stopped);
        }

        private void Continue()
        {
            isGamePaused = false;
            pauseCanvas.enabled = false;
            timeScaler.DeactivateTimeScale(TimeScale.Stopped);
        }

        private void OnQuitButtonClicked()
        {
            isGamePaused = false;
            globalController.GoToMenu();
        }
    }
}