﻿using System;
using System.Collections;
using System.Configuration;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace Game
{
    //Author: Antony Ménard
    [Findable(R.S.Tag.LoadingScreenController)]
    public class LoadingScreenController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI tipTextBox;
        [SerializeField] private TextMeshProUGUI buttonTextBox;
        [SerializeField] private TextAsset tipsFile;
        [SerializeField] private float loadingScreenTimeInSeconds = 2;

        private const int INITIAL_LENGTH = 1;

        private MusicController musicController;
        private TimeScalerController timeScaler;
        private GameController gameController;
        private string[] tipsArray;
        private int arrayLength;
        private Canvas loadingScreenCanvas;
        private bool canBePaused;

        public bool CanBePaused => canBePaused;

        private void Awake()
        {
            canBePaused = false;
            musicController = Finder.MusicController;
            timeScaler = Finder.TimeScalerController;
            arrayLength = INITIAL_LENGTH;
            tipsArray = new string[arrayLength];
            loadingScreenCanvas = GetComponent<Canvas>();
            loadingScreenCanvas.enabled = false;
            SeparateTips(tipsFile.text);
            ButtonTextActivation(false);
        }

        public IEnumerator WaitForLoadingScreen()
        {
            canBePaused = false;
            ButtonTextActivation(false);
            SetTipsOnCanvas();
            TipActivation(true);
            timeScaler.ActivateTimeScale(TimeScale.Stopped);
            
            loadingScreenCanvas.enabled = true;

                        
            yield return new WaitForSecondsRealtime(loadingScreenTimeInSeconds);
            
            StartCoroutine(WaitForButton());
        }

        private IEnumerator WaitForButton()
        {
            TipActivation(false);
            ButtonTextActivation(true);
            while (!IsButtonPressed())
                yield return null;
            
            loadingScreenCanvas.enabled = false;

            musicController.NextMusic();
            timeScaler.DeactivateTimeScale(TimeScale.Stopped);
            canBePaused = true;
            Finder.GameController.StartNewCountdown();
        }
        
        private bool IsButtonPressed()
        {
            return Input.anyKeyDown;
        }
        
        private void SetTipsOnCanvas()
        {
            var randomTipIndex = Random.Range(0, tipsArray.Length);
            tipTextBox.text = tipsArray[randomTipIndex];
        }

        private void SeparateTips(string tips)
        {
            var chariotReturnIndex = 0;
            var newTip = "";
            var currentArrayIndex = 0;

            while (tips.Contains("\n"))
            {
                chariotReturnIndex = tips.IndexOf("\n");
                newTip = tips.Substring(0, chariotReturnIndex - 1);

                AddLengthToArray();
                tipsArray[currentArrayIndex] = newTip;
                tips = tips.Remove(0, newTip.Length + 2);
                currentArrayIndex++;
            }
        }
                
        private void AddLengthToArray()
        {
            var tempArray = tipsArray;
            tipsArray = new string[++arrayLength];

            for (var i = 0; i < tempArray.Length; i++)
            {
                tipsArray[i] = tempArray[i];
            }
        }

        private void TipActivation(bool activate)
        {
            tipTextBox.enabled = activate;
        }
        
        private void ButtonTextActivation(bool activate)
        {
            buttonTextBox.enabled = activate;
        }
    }
}