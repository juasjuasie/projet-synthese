﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Xml.Serialization;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Gabriel St-Laurent-Recoura, Antony Ménard
    [Findable(R.S.Tag.PlayerController)]
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private KeyCode jumpKey = KeyCode.Z;
        [SerializeField] private KeyCode slowTimeKey = KeyCode.Mouse1;
        [SerializeField] private KeyCode fireButton = KeyCode.Mouse0;
        [SerializeField] [Range(0,100)] private int initialEnergy = 50;
        [SerializeField] [Range(0, 100)] private int energyDecreaseEachTick;
        [SerializeField] [Range(0, 1)] private float secBetweenTicks;
        [SerializeField] [Range(0, 1000)] private float coyoteTimeDuration = 0.1f;
        [SerializeField] private PlayerSfxHandler playerSfxHandler;


        public const int MAX_ENERGY = 100;

        private bool playerIsActivated;
        private int energy;
        private PlayerMover playerMover;
        private bool onGround;
        private bool canDoubleJump;
        private PlayerDeathEventChannel playerDeathEventChannel;
        private TimeScaleChangedEventChannel timeScaleChangedEventChannel;
        private Weapon weapon;
        private RailConnector railConnector;
        private bool isConnectedToRail;
        private int nbNormalGravityZoneInHitbox;
        private int nbInvertedGravityZoneInHitbox;
        private int nbOfConctacts;
        private TimeScalerController timeScaler;
        private ParticleController particleController;
        private IEnumerator removeEnergyRoutine;

        public int Energy => energy;

        public bool PlayerIsActivated => playerIsActivated;

        public RailConnector RailConnector => railConnector;

        private void Awake()
        {
            particleController = GetComponentInChildren<ParticleController>();
            playerIsActivated = Finder.GameController.IsRespawn;
            removeEnergyRoutine = RemoveEnergy();
            playerSfxHandler = GetComponentInChildren<PlayerSfxHandler>();
            Finder.GameController.PlayerIsAlive();
            timeScaler = Finder.TimeScalerController;
            timeScaleChangedEventChannel = Finder.TimeScaleChangedEventChannel;
            playerDeathEventChannel = Finder.PlayerDeathEventChannel;
            playerMover = GetComponent<PlayerMover>();
            weapon = GetComponentInChildren<Weapon>();
            energy = initialEnergy;
            isConnectedToRail = false;
            nbInvertedGravityZoneInHitbox = 0;
            nbOfConctacts = 0;
            railConnector = new RailConnector(this, playerMover);
        }

        private void OnEnable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged += OnTimeScaleChanged;
        }

        private void OnDisable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged -= OnTimeScaleChanged;
        }

        private void Update()
        {
            if (playerIsActivated)
            {
                if (!isConnectedToRail)
                {
                    CheckForJump();
                }
    
                else
                    SlideOnRail();
    
                if (Input.GetKeyDown(fireButton))
                {
                    weapon.Fire();
                    playerSfxHandler.PlayFireSfx();
                }
    
                if (Input.GetKeyDown(slowTimeKey) && energy > 0)
                {
                    timeScaler.ActivateTimeScale(TimeScale.Slowed);
                }
                if(energy <= 0 || !Input.GetKey(slowTimeKey))
                {
                    timeScaler.DeactivateTimeScale(TimeScale.Slowed);
                }
            }
        }

        private IEnumerator RemoveEnergy()
        {
            while (energy > 0)
            {
                energy -= energyDecreaseEachTick;
                yield return new WaitForSecondsRealtime(secBetweenTicks);
            }
        }

        private void OnTimeScaleChanged(TimeScale newTimeScale)
        {
            if (newTimeScale == TimeScale.Slowed)
            {
                StartCoroutine(removeEnergyRoutine);
                playerIsActivated = true;
            }
            else if (newTimeScale == TimeScale.Frozen)
            {
                StopCoroutine(removeEnergyRoutine);
                playerIsActivated = false;
            }
            else if (newTimeScale == TimeScale.Stopped)
            {
                StopCoroutine(removeEnergyRoutine);
                playerIsActivated = false;
            }
            else
            {
                StopCoroutine(removeEnergyRoutine);
                playerIsActivated = true;
            }
        }

        private void SlideOnRail()
        {
            playerMover.TranslateForward();
            playerSfxHandler.PlayGrindSfx();
        }

        private void CheckForJump()
        {
            if (Input.GetKeyDown(jumpKey) && (onGround || canDoubleJump))
            {
                if (!onGround)
                    canDoubleJump = false;
                
                playerMover.Jump();
                playerSfxHandler.PlayJumpSfx();
                onGround = false;
            }
        }

        private void ResetDoubleJump()
        {
            canDoubleJump = true;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
              nbOfConctacts++;
            var contact = other.GetContact(0).normal;
            if (other.gameObject.GetComponent<PierceableObject>() == null) 
                CheckIfContactIsFront(contact);
            CheckIfContactIsDown(contact);
        }

        private void CheckIfContactIsFront(Vector2 contact)
        {
            if (contact.x < 0)
            {
                Kill();
            }
        }
        
        private void CheckIfContactIsDown(Vector2 contact)
        {
            if (!playerMover.IsGravityInverted && contact.y > 0)
            {
                ConctactWithGround();
            }
            else if (playerMover.IsGravityInverted && contact.y < 0)
            {
                ConctactWithGround();
            }
        }

        private void ConctactWithGround()
        {
            if (!onGround)
                playerSfxHandler.PlayFloorHitSfx();
            onGround = true;
            canDoubleJump = true;
        }

        public void Kill()
        {
            playerSfxHandler.PlayDeathSfx();
            playerDeathEventChannel.Publish();
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            nbOfConctacts--;
            if (nbOfConctacts <= 0)
                StartCoroutine(StartCoyoteTime());
        }
        
        private IEnumerator StartCoyoteTime()
        {
            yield return new WaitForSeconds(coyoteTimeDuration);
            onGround = false;
        }

        public void AddEnergy(int energyToAdd)
        {
            if (energy + energyToAdd <= MAX_ENERGY)
                energy += energyToAdd;
            else
                energy = MAX_ENERGY;


        }
        
        public void ConnectToNormalGravityZone()
        {
            nbNormalGravityZoneInHitbox++;
            if (nbNormalGravityZoneInHitbox >= 1 && playerMover.IsGravityInverted)
            {
                playerSfxHandler.PlayGravityChangedSfx();
                playerMover.SetNormalGravity();
            }
        }

        public void DisconnectToNormalGravityZone()
        {
            nbNormalGravityZoneInHitbox--;
        }

        public void ConnectToInvertedGravityZone()
        {
            nbInvertedGravityZoneInHitbox++; 
            if (nbInvertedGravityZoneInHitbox >= 1 && !playerMover.IsGravityInverted)
            {
                playerSfxHandler.PlayGravityChangedSfx();
                playerMover.SetInvertedGravity();
                AchievementHandler.CheckForSpecificAchievement(Achievement.Gravity);
            }
        }

        public void DisconnectToInvertedGravityZone()
        {
            nbInvertedGravityZoneInHitbox--;
        }
        
        //Author : Nicolas Bisson
        public void OnRailConnection()
        {
            isConnectedToRail = true;
            playerSfxHandler.PlayGrindSfx();
            particleController.ActivateParticles(true);
        }

        //Author : Nicolas Bisson
        public void OnRailDisconnection()
        {
            isConnectedToRail = false;
            playerSfxHandler.StopGrindSfx();
            particleController.ActivateParticles(false);
            ResetDoubleJump();
        }

        //Author : Nicolas Bisson
        public void UseJumpPad(float bounceForce)
        {
            playerMover.Bounce(bounceForce);
            ResetDoubleJump();
        }
    }
}