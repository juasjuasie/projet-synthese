﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.Serialization;

namespace Game
{
    //Author : Nicolas Bisson
    public class LaserSight : MonoBehaviour
    {
        [SerializeField] public int maxLenght = 60;

        public void SetLaserLenght()
        {
            transform.localScale = RaycastCalculator.GetFirstUnpierceableHitLenght
                (transform.position, transform.right, maxLenght, out _);
        }
    }
}