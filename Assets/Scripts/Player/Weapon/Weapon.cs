﻿using System;
using System.Net;
using UnityEngine;
using Harmony;

namespace Game
{
    //Author : Nicolas Bisson
    public class Weapon : MonoBehaviour
    {
        [SerializeField] protected GameObject shotSpawnPointTop;
        [SerializeField] protected GameObject shotSpawnPointBottom;
        [SerializeField] protected GameObject laserSightSpawnPoint;
        [SerializeField] private GameObject projectile;
        [SerializeField] private GameObject projectilePreview;

        private Camera camera;
        private PlayerController playerController;

        private LaserSight laserSight;

        private void Awake()
        {
            camera = GameObjectFinder.Camera.GetComponent<Camera>();
            playerController = GetComponentInParent<PlayerController>();
        }

        private void Update()
        {
            if (playerController.PlayerIsActivated)
            {
                FollowMouse();
                //The laser sight adjustement are called from here instead of the laser sight component
                //to make sure it is always up to date to the weapon rotation
                ShowLaserSight();
            }
        }

        private void FollowMouse()
        {
            Vector3 weaponPosition = camera.WorldToScreenPoint(transform.position);
            var direction = Input.mousePosition - weaponPosition;
            var angle = Vector3.SignedAngle(Vector3.right, direction, Vector3.forward);
            transform.rotation = Quaternion.Euler(0, 0, angle);
        }

        public void Fire()
        {
            var rotation = transform.rotation;
            //Shoot two projectile to give the shot a more laser-like feeling (An edge will only stop a part of the shot)
            Instantiate(projectile, shotSpawnPointTop.transform.position, rotation);
            Instantiate(projectile, shotSpawnPointBottom.transform.position, rotation);
        }
        
        private void ShowLaserSight()
        {
            if (laserSight == null)
                laserSight = CreateLaserSight();
            else
                laserSight.SetLaserLenght();
        }

        private LaserSight CreateLaserSight()
        {
            return Instantiate(projectilePreview, laserSightSpawnPoint.transform).GetComponent<LaserSight>();
        }
    }
}