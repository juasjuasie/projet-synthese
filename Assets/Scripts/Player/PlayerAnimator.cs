﻿using DG.Tweening;
using UnityEngine;

namespace Game
{
    public class PlayerAnimator : MonoBehaviour
    {
        [SerializeField] private GameObject wheel;

        private Vector3 vector3ToRotateTo;
        
        private void Awake()
        {
            vector3ToRotateTo = new Vector3(0,0,-360);
            RotateWheel();
        }
        
        private void RotateWheel()
        {
            wheel.transform.DORotate(vector3ToRotateTo, 1, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
        }

        private void OnDestroy()
        {
            wheel.transform.DOKill();
        }
    }
}