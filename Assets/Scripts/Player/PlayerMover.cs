﻿using System;
using System.Security.Cryptography.X509Certificates;
using Harmony;
using UnityEngine;
using UnityEngine.AI;

namespace Game
{
    //Author: Gabriel St-Laurent-Recoura, Nicolas Bisson
    public class PlayerMover : MonoBehaviour
    {
        [SerializeField] [Range(0,100)]private float speed = 300;
        [SerializeField] [Range(0,100)]private float jumpForce = 500;

        private const int MAX_RAIL_DETECTION_LENGHT = 2;
        private const float ROTATION_ON_INVERTED_GRAVITY = 180f;

        private Rigidbody2D rigidbody2D;
        private bool isGravityInverted;
        private float originalGravity;
        private GameObject visual;
        private Collider2D collider;

        public bool IsGravityInverted
        {
            get => isGravityInverted;
            private set
            {
                if (value != isGravityInverted)
                {    
                    visual.transform.RotateAround(collider.bounds.center, Vector3.right, ROTATION_ON_INVERTED_GRAVITY);
                    isGravityInverted = value;
                }
            }
        }

        private void Awake()
        {
            rigidbody2D = GetComponentInChildren<Rigidbody2D>();
            isGravityInverted = false;
            originalGravity = rigidbody2D.gravityScale;
            visual = GetComponentInChildren<SpriteRenderer>().gameObject.Parent();
            collider = GetComponentInChildren<Collider2D>();
        }

        private void Start()
        {
            SetToBaseMovementForce();
        }

        public void Jump()
        {
            SetVerticalForce(jumpForce);
        }

        public void TranslateForward()
        {
            transform.Translate(Time.deltaTime * speed * Vector3.right);
        }

        public void StabilizeVerticalMovement()
        {
            Vector2 stabilizedVelocity = rigidbody2D.velocity;
            stabilizedVelocity.y = 0;
            rigidbody2D.velocity = stabilizedVelocity;
        }

        public void SetToBaseMovementForce()
        {
            //The player has no friction in the game
            rigidbody2D.velocity = rigidbody2D.velocity + new Vector2(speed, 0f);
        }

        public void SetPlayerToOnRailState(Rail rail)
        {
            var railCollider = rail.GetComponentInChildren<Collider2D>();
            TryToConnectToContactPoint(Vector2.down, railCollider);
            var railRotation = rail.transform.rotation;
            transform.rotation = railRotation;
            SetToKinematic();
        }

        private void TryToConnectToContactPoint(Vector2 raycastDirection, Collider2D collider)
        {
             var position = transform.position;
            var raycast = RaycastCalculator.GetFirstSpecificColliderHit
                (position + Vector3.up * MAX_RAIL_DETECTION_LENGHT, raycastDirection, MAX_RAIL_DETECTION_LENGHT * 2, collider);
            
            if (raycast != null && raycast.Value.point != Vector2.zero)
                transform.position = raycast.Value.point;
        }

        public void SetToKinematic()
        {
            rigidbody2D.velocity = Vector2.zero;
            rigidbody2D.bodyType = RigidbodyType2D.Kinematic;
            rigidbody2D.useFullKinematicContacts = true;
        }

        public void StopPlayerOnRailState()
        {
            transform.rotation = Quaternion.identity;
            rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
            SetToBaseMovementForce();
        }

        public bool IsFalling()
        {
            return rigidbody2D.velocity.y <= 0;
        }

        public void SetInvertedGravity()
        {
            IsGravityInverted = true;
            rigidbody2D.gravityScale = -originalGravity;
        }
        
        public void SetNormalGravity()
        {
            IsGravityInverted = false;
            rigidbody2D.gravityScale = originalGravity;
        }

        public void SetVerticalForce(float force)
        {
            StabilizeVerticalMovement();
            if (!IsGravityInverted)
            {
                rigidbody2D.velocity = rigidbody2D.velocity + new Vector2(0f, force);
            }
            else
            {
                rigidbody2D.velocity = rigidbody2D.velocity + new Vector2(0f, -force);
            }
        }

        public void Bounce(float bounceForce)
        {
            SetVerticalForce(bounceForce);
        }
    }
}