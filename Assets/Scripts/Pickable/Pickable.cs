﻿using System;
using UnityEngine;

namespace Game
{
    //Author: Antony Ménard
    public abstract class Pickable : MonoBehaviour
    {
        [SerializeField] protected PickableSfxHandler sfxHandler;
        [SerializeField][Range(0, 100)] protected float particleShowTime = 0f;
        
        protected PlayerController player;
        
        private ParticleController particleController;
        private ISensor<PlayerController> sensorPlayer;
        
        protected void Awake()
        {
            particleController = GetComponentInChildren<ParticleController>();
            sensorPlayer = GetComponentInChildren<Sensor>().For<PlayerController>();
        }

        protected void Start()
        {
            player = GameObjectFinder.Player.GetComponent<PlayerController>();
        }

        protected virtual void OnEnable()
        {
            sensorPlayer.OnSensedObject += OnPlayerSensed;
        }
        
        protected virtual void OnDisable()
        {
            sensorPlayer.OnSensedObject -= OnPlayerSensed;
        }

        protected virtual void OnPlayerSensed(PlayerController player)
        {
            particleController.ActivateParticles(true);
            particleController.ChangeParent(transform.parent, particleShowTime);
            sfxHandler.PlayPickSfx();
            sfxHandler.transform.parent = transform.parent;
            Destroy(gameObject);
        }
    }
}