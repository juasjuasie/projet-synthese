﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Antony Ménard, Gabriel St-Laurent-Recoura
    public class StopTimeSphere : Pickable
    {
        [SerializeField] [Range(1, 100)] public int nbSecondStopTime = 0;
        
        private TimeScalerController timeScaler;
        private TimeScaleChangedEventChannel timeScaleChangedEventChannel;
        private Renderer spriteRenderer;
        private CountdownController countdownController;
        private int countdownIndex;
        private IEnumerator countdownRoutine;
        private bool isActivated;
        private new void Awake()
        {
            countdownIndex = nbSecondStopTime;
            base.Awake();
            countdownController = Finder.CountdownController;
            timeScaler = Finder.TimeScalerController;
            timeScaleChangedEventChannel = Finder.TimeScaleChangedEventChannel;
            spriteRenderer = GetComponentInChildren<Renderer>();
            spriteRenderer.enabled = true;
            countdownRoutine = CountdownRoutine();
            isActivated = false;
        }
        
        protected override void OnEnable()
        {
            base.OnEnable();
            timeScaleChangedEventChannel.OnTimeScaleChanged += OnTimeScaleChanged;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            timeScaleChangedEventChannel.OnTimeScaleChanged -= OnTimeScaleChanged;
        }
        
        private void OnTimeScaleChanged(TimeScale newTimeScale)
        {
            if (newTimeScale == TimeScale.Suspended)
            {
                if(isActivated)
                    StartCoroutine(countdownRoutine);
            }
            else
            {
                if(isActivated)
                    StopCoroutine(countdownRoutine);
            }
        }
        
        protected override void OnPlayerSensed(PlayerController player)
        {
            isActivated = true;
            timeScaler.ActivateTimeScale(TimeScale.Suspended);
            spriteRenderer.enabled = false;
        }

        private IEnumerator CountdownRoutine()
        {
            countdownController.ShowCountdown(countdownIndex);
            while (countdownIndex > 0)
            {
                yield return new WaitForSecondsRealtime(1);
                countdownIndex--;
                countdownController.UpdateCountdown(countdownIndex);
            }
            countdownController.HideCountdown();
            timeScaler.DeactivateTimeScale(TimeScale.Suspended);
        }
    }
}