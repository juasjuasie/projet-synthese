﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Antony Ménard
    public class EnergySphere : Pickable
    {
        [SerializeField] [Range(0, 100)] private int energyValue = 5;
        [SerializeField] [Range(0, 100)] private uint scoreValue = 100;

        private GameController gameController;
        private void Awake()
        {
            base.Awake();
            gameController = Finder.GameController;
        }
        
        protected override void OnPlayerSensed(PlayerController playerHurtbox)
        {
            player.AddEnergy(energyValue);
            gameController.AddScore(scoreValue);
            base.OnPlayerSensed(playerHurtbox);
        }
    }
}