﻿using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game
{
    //Author: Eduardo Breton
    [Findable(R.S.Tag.GameHUD)]
    public class GameHUDController : MonoBehaviour
    {
        [SerializeField] private Text scoreText;
        [SerializeField] private HorizontalLayoutGroup energyBarLayout;
        [SerializeField] private string scoreFormat = "Score : {0:00000000}";
        [SerializeField] private int maxEnergyParts = 35;
        [SerializeField] private uint maximalScoreValue = 99999999;
        [SerializeField] private uint minimalScoreValue = 0;

        private int maxEnergy;
        private PlayerGainedScoreEventChannel playerGainedScoreEventChannel;
        private GameController gameController;
        private PlayerController playerController;
        private List<Transform> energyParts;
        private Canvas hudCanvas;

        public Canvas HudCanvas
        {
            get => hudCanvas;
            set => hudCanvas = value;
        }

        private void Awake()
        {
            playerGainedScoreEventChannel = Finder.PlayerGainedScoreEventChannel;
            gameController = Finder.GameController;
            FindEnergyParts();
            UpdateHUDEnergy();
            hudCanvas = GetComponent<Canvas>();
        }

        private void Update()
        {
            UpdateHUDEnergy();
        }

        private void OnEnable()
        {
            playerGainedScoreEventChannel.OnScoreGained += UpdateHUDScore;
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        private void OnDisable()
        {
            if (playerGainedScoreEventChannel != null)
                playerGainedScoreEventChannel.OnScoreGained -= UpdateHUDScore;

            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (GameObjectFinder.Player != null)
            {
                var player = GameObjectFinder.Player;
                playerController = player.GetComponent<PlayerController>();
                maxEnergy = PlayerController.MAX_ENERGY;
            }
        }

        private void UpdateHUDScore()
        {
            var score = gameController.Score;
            score = (uint) Mathf.Clamp(score, minimalScoreValue, maximalScoreValue);
            scoreText.text = string.Format(scoreFormat, score);
        }

        private void UpdateHUDEnergy()
        {
            if (playerController)
            {
                var currentPercentage = (playerController.Energy * 100) / maxEnergy;
                var numberOfActiveBars = (currentPercentage * maxEnergyParts) / 100;
                RefreshEnergyParts(numberOfActiveBars);
            }
        }

        private void FindEnergyParts()
        {
            energyParts = energyBarLayout.GetComponentsInChildren<Transform>().ToList();
        }

        private void RefreshEnergyParts(int nbActiveBars)
        {
            for (int i = energyParts.Count-1; i >= 0; i--)
            {
                energyParts[i].gameObject.SetActive(false);
            }

            for (int i = 0; i <= nbActiveBars; i++)
            {
                energyParts[i].gameObject.SetActive(true);
            }
        }
    }
}