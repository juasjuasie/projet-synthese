﻿using System.Collections;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.Jobs;

namespace Game
{
    //Author: Gabriel St-Laurent-Recoura
    [Findable(R.S.Tag.CountdownCanvas)]
    public class CountdownController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI countDownText;

        private Canvas canvas;

        private void Awake()
        {
            canvas = GetComponentInChildren<Canvas>();
            HideCountdown();
        }

        public void HideCountdown()
        {
            canvas.enabled = false;
        }
        
        public void ShowCountdown(int startCount)
        {
            countDownText.text = startCount.ToString();
            canvas.enabled = true;
        }

        public void UpdateCountdown(int newCount)
        {
            countDownText.text = newCount.ToString();
        }
    }
}