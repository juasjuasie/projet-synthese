﻿using Harmony;

//Author : Gabriel St-Laurent-Recoura
namespace Game
{
    public enum Level
    {
        Level1 = 0,
        Level2,
        Level3,
        Level4,
        Level5
    }
}