﻿using System;
using System.Collections;
using System.Security;
using Game;
using Harmony;
using SpriteGlow;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    //Author: Gabriel St-Laurent-Recoura, Eduardo Breton
    [Findable(R.S.Tag.GameController)]
    public class GameController : MonoBehaviour
    {
        [SerializeField] private int countdownDuration;

        private GlobalController globalController;
        private SceneReference currentLevel;
        private VictoryScreenController victoryScreenController;
        private TimeScalerController timeScaler;
        private CountdownController countdownController;
        private MusicController musicController;
        
        private PlayerDeathEventChannel playerDeathEventChannel;
        private PlayerGainedScoreEventChannel playerGainedScoreEventChannel;
        private GameHasEndedFormallyEventChannel gameHasEndedFormallyEventChannel;
        private TimeScaleChangedEventChannel timeScaleChangedEventChannel;

        private uint score;
        private bool playerIsAlive;
        private bool hasDied;
        private bool hasDiedThisLevel;
        private bool checkpointActivated;
        private Vector3 checkpointPosition;
        private int countdownIndex;
        private IEnumerator countdownRoutine;
        private bool isRespawn;

        public uint Score => score;

        public bool IsRespawn => isRespawn;

        private void Awake()
        {
            isRespawn = false;
            countdownController = Finder.CountdownController;
            timeScaleChangedEventChannel = Finder.TimeScaleChangedEventChannel;
            timeScaler = Finder.TimeScalerController;
            globalController = Finder.GlobalController;
            victoryScreenController = Finder.VictoryScreenController;
            playerDeathEventChannel = Finder.PlayerDeathEventChannel;
            playerGainedScoreEventChannel = Finder.PlayerGainedScoreEventChannel;
            gameHasEndedFormallyEventChannel = Finder.GameHasEndedFormallyEventChannel;
            musicController = Finder.MusicController;
            hasDied = false;
            hasDiedThisLevel = false;
            countdownRoutine = CountdownRoutine();
        }

        private void OnEnable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged += OnTimeScaleChanged;
            playerDeathEventChannel.OnPlayerDeath += OnPlayerDeath;
            gameHasEndedFormallyEventChannel.OnGameEnded += OnGameEnded;
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        
        private void OnDisable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged -= OnTimeScaleChanged;
            playerDeathEventChannel.OnPlayerDeath -= OnPlayerDeath;
            gameHasEndedFormallyEventChannel.OnGameEnded += OnGameEnded;
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        public void NextLevel()
        {
            AchievementHandler.CheckSingleLevelEndAchievement(hasDiedThisLevel);
            if (!globalController.IsOnLastLevel())
            {
                isRespawn = false;
                checkpointActivated = false;
                globalController.ChangeToNextLevel();
                hasDiedThisLevel = false;
            }
            else
            {
                timeScaler.ActivateTimeScale(TimeScale.Stopped);
                AchievementHandler.CheckForEndGameAchievement(hasDied, score);
                victoryScreenController.enabled = true;
                victoryScreenController.Show();
            }
        }
        
        private void OnTimeScaleChanged(TimeScale newTimeScale)
        {
            if (newTimeScale == TimeScale.Frozen)
            {
                StartCoroutine(countdownRoutine);
            }
            else
            {
                StopCoroutine(countdownRoutine);
            }
        }
        
        private void OnPlayerDeath()
        {
            if (playerIsAlive)
            {
                isRespawn = true;
                playerIsAlive = false;
                //Reduce the score of the player by half on death
                score /= 2;
                playerGainedScoreEventChannel.Publish();
                hasDied = true;
                hasDiedThisLevel = true;
                globalController.ReloadCurrentLevel();
            }
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (checkpointActivated)
            {
                GameObjectFinder.Player.transform.position = checkpointPosition;
            }
        }

        private void OnGameEnded()
        {
            var activeAccount = Finder.ActiveAccount;
            var scoreSaver = Finder.ScoreSaver;
            var scoreEntity = new ScoreEntity(activeAccount.AccountEntity, score);
            scoreSaver.SaveScore(scoreEntity, activeAccount.AccountEntity);
        }

        public void AddScore(uint scoreToAdd)
        {
            score += scoreToAdd;
            playerGainedScoreEventChannel.Publish();
        }

        public void PlayerIsAlive()
        {
            playerIsAlive = true;
        }

        public void SetCheckpoint(Vector3 newCheckpointPosition)
        {
            checkpointActivated = true;
            checkpointPosition = newCheckpointPosition;
            musicController.SaveCurrentMusicTime();
        }

        public void StartNewCountdown()
        {
            countdownRoutine = CountdownRoutine();
            countdownIndex = countdownDuration;
            timeScaler.ActivateTimeScale(TimeScale.Frozen);
        }

        private IEnumerator CountdownRoutine()
        {
            countdownController.ShowCountdown(countdownIndex);
            while (countdownIndex > 0)
            {
                yield return new WaitForSecondsRealtime(1);
                countdownIndex--;
                countdownController.UpdateCountdown(countdownIndex);
            }
            countdownController.HideCountdown();
            timeScaler.DeactivateTimeScale(TimeScale.Frozen);
        }
    }
}