﻿using UnityEngine;

namespace Game
{
    //Author: Antony Ménard, Gabriel St-Laurent-Recoura, Nicolas Bisson
    public class Laser : MonoBehaviour
    {
        public const int MAX_LENGHT = 60;
        
        private ParticleController particleController;
        private ISensor<PlayerController> sensorPlayer;
        
        private new void Awake()
        {
            sensorPlayer = GetComponentInChildren<Sensor>().For<PlayerController>();
            ChangeLaserScale();
        }

        protected void OnEnable()
        {
            sensorPlayer.OnSensedObject += OnPlayerSensed;
        }
        
        protected void OnDisable()
        {
            sensorPlayer.OnSensedObject -= OnPlayerSensed;
        }
        
        private void OnPlayerSensed(PlayerController player)
        {
            player.Kill();
        }

        private void ChangeLaserScale()
        {
            transform.localScale = RaycastCalculator.GetFirstUnpierceableHitLenght
                (transform.position, transform.right, MAX_LENGHT, out _);
        }
    }
}