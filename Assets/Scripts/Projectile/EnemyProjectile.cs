﻿using System;
using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Gabriel St-Laurent-Recoura, Nicolas Bisson
    public abstract class EnemyProjectile : MonoBehaviour
    {
        [SerializeField][Range(1, 100)] protected float projectileSpeed = 50f;
        [SerializeField][Range(0, 100)] protected float particleShowTime = 0f;

        private ParticleController particleController;
        private ISensor<PlayerController> sensorPlayer;
        private ISensor<Ground> sensorGround;
        
        protected void Awake()
        {
            particleController = GetComponentInChildren<ParticleController>();
            sensorPlayer = GetComponentInChildren<Sensor>().For<PlayerController>();
            sensorGround = GetComponentInChildren<Sensor>().For<Ground>();
        }

        protected void OnEnable()
        {
            sensorPlayer.OnSensedObject += OnPlayerSensed;
            sensorGround.OnSensedObject += OnGroundSensed;
        }
        
        protected void OnDisable()
        {
            sensorPlayer.OnSensedObject -= OnPlayerSensed;
            sensorGround.OnSensedObject -= OnGroundSensed;
        }
        
        protected virtual void Update()
        {
            transform.Translate(Time.deltaTime * projectileSpeed * Vector3.right);
        }
        
        private void OnPlayerSensed(PlayerController player)
        {
            player.Kill();
        }
        
        private void OnGroundSensed(Ground ground)
        {
            particleController.ActivateParticles(true);
            particleController.ChangeParent(transform.parent, particleShowTime);

              Destroy(gameObject);
        }
    }
}