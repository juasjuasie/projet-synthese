﻿using System;
using System.Collections;
using UnityEngine;

namespace Game
{
    //Author: Antony Ménard
    public class Missile : EnemyProjectile, IShotable
    {
        [SerializeField] private int missileAcceleratedSpeed = 0;
        [SerializeField] private MissileSfxHandler sfxHandler;
        
        private const int DISTANCE_START_ACCELERATION = 2;
        private const int PLAYER_POSITION_OFFSET = 50;
        
        private GameObject player;
        private bool hasNotAccelerated;
        
        private new void Awake()
        {
            base.Awake();
            player = GameObjectFinder.Player;
            hasNotAccelerated = true;
        }

        private void Start()
        {
            sfxHandler.PlayLockOnSfx();
        }

        protected override void Update()
        {
            SetPosition();
            SetShotDirection();

            if (IsMissileBehindPlayer())
                projectileSpeed = missileAcceleratedSpeed;
        }

        private void SetPosition()
        {
            transform.Translate(projectileSpeed * Time.deltaTime * Vector3.right);
        }

        private bool IsMissileBehindPlayer()
        {
            return Mathf.Abs(player.transform.position.y - transform.position.y) <= DISTANCE_START_ACCELERATION
                   && hasNotAccelerated;
        }
        
        private void SetShotDirection()
        {
            var zRotation = GetShotDirection();
            var target = Quaternion.Euler(0, 0, zRotation);
            transform.rotation = target;
        }
        
        private float GetShotDirection()
        {
            var missileInCameraPosition = Camera.main.WorldToScreenPoint(transform.position);
            var playerInCameraPosition = Camera.main.WorldToScreenPoint(player.transform.position);
            playerInCameraPosition.y += PLAYER_POSITION_OFFSET;

            var direction = playerInCameraPosition - missileInCameraPosition;
            var angle = Vector3.SignedAngle(Vector3.right, direction, Vector3.forward);
        
            return angle;
        }

        public void OnShot()
        {
            StartCoroutine(WaitForEndOfFrameToDestroy());
        }

        private IEnumerator WaitForEndOfFrameToDestroy()
        {
            yield return new WaitForEndOfFrame();
            Destroy(gameObject);
        }
    }
}