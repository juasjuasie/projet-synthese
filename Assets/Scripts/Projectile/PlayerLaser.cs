﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game
{
    //Author : Nicolas Bisson
    public class PlayerLaser : MonoBehaviour
    {
        [SerializeField] private float shotDuration = 0.03f;
        [SerializeField] public int maxLenght = 60;
        [SerializeField][Range(0, 100)] protected float particleShowTime = 0f;

        private ParticleController particleController;
        private static List<GameObject> shotableDetectedByLasersThisFrame = new List<GameObject>();
        
        protected void Awake()
        {
            particleController = GetComponentInChildren<ParticleController>();
            Fire();
        }

        private void Fire()
        {
            transform.localScale = RaycastCalculator.GetFirstUnpierceableHitLenght
                (transform.position, transform.right, maxLenght, out var objectHit);
            
            if (objectHit != null)
                OnCollision(objectHit.Value);
            
            Destroy(gameObject, shotDuration);
        }

        private void OnCollision(RaycastHit2D firstObjectHit)
        {
            particleController.ActivateParticles(true);
            particleController.ChangeParent(transform.parent, particleShowTime);
            var detectedObject = firstObjectHit.collider.Parent();
            TriggerDetectedObjectCollision(detectedObject);
        }

        private void TriggerDetectedObjectCollision(GameObject detectedObject)
        {
            if (detectedObject != null)
            {
                var shotable = detectedObject.GetComponent<IShotable>();
                if (shotable != null)
                {
                    var shotableHasAlreadyBeenTriggered = false;
                    foreach (var detectedShotable in shotableDetectedByLasersThisFrame)
                    {
                        if (detectedObject == detectedShotable)
                        {
                            shotableHasAlreadyBeenTriggered = true;
                        }
                    }

                    if (!shotableHasAlreadyBeenTriggered)
                    {
                        shotable.OnShot();

                        if (shotableDetectedByLasersThisFrame.Count == 0)
                        {
                            StartCoroutine(ResetObjectDetectedList());
                        }

                        shotableDetectedByLasersThisFrame.Add(detectedObject);
                    }
                }
            }
        }

        private IEnumerator ResetObjectDetectedList()
        {
            yield return new WaitForEndOfFrame();
            shotableDetectedByLasersThisFrame.Clear();
        }
    }
}