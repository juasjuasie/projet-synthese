﻿using UnityEngine;

namespace Game
{
    //Author : Gabriel St-Laurent-Recoura
    public class TimeToBeBoldZone : Zone
    {
        [TextArea]
        [SerializeField] private string BeBoldZoneText;
        
        private TimeToBeBoldController timeToBeBoldController;
        
        private void Awake()
        {
            base.Awake();
            timeToBeBoldController = GameObjectFinder.TimeToBeBoldCanvas.GetComponent<TimeToBeBoldController>();
            timeToBeBoldController.ChangeTimeToBeBoldText(BeBoldZoneText);
        }

        protected override void OnPlayerSensed(PlayerController player)
        {
            timeToBeBoldController.EnableCanvas();
        }
        
        protected override void OnPlayerUnsensed(PlayerController player)
        {
            timeToBeBoldController.DisableCanvas();
        }
    }
}