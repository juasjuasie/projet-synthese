﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Gabriel St-Laurent-Recoura
    public abstract class Zone : Flag
    {
        protected void OnEnable()
        {
            base.OnEnable();
            sensorPlayer.OnUnsensedObject += OnPlayerUnsensed;
        }
        
        protected void OnDisable()
        {
            base.OnDisable();
            sensorPlayer.OnUnsensedObject -= OnPlayerUnsensed;
        }
        

        protected abstract void OnPlayerUnsensed(PlayerController player);
    }
}