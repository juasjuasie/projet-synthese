﻿using System;
using Game;
using UnityEngine;

namespace Game
{
    //Author : Gabriel St-Laurent-Recoura
    public class NormalGravityZone : Zone
    {
        private ISensor<PlayerController> sensorPlayer;

        protected override void OnPlayerSensed(PlayerController player)
        {
            player.ConnectToNormalGravityZone();
        }
        
        protected override void OnPlayerUnsensed(PlayerController player)
        {
            player.DisconnectToNormalGravityZone();
        }
    }
}