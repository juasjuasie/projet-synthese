﻿using System;
using Game;
using UnityEngine;

namespace Game
{
    //Author : Gabriel St-Laurent-Recoura
    public class InvertedGravityZone : Zone
    {
        private ISensor<PlayerController> sensorPlayer;

        protected override void OnPlayerSensed(PlayerController player)
        {
            player.ConnectToInvertedGravityZone();
        }
        
        protected override void OnPlayerUnsensed(PlayerController player)
        {
            player.DisconnectToInvertedGravityZone();
        }
    }
}