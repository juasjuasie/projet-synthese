﻿namespace Game
{
    //Author: Antony Ménard
    public enum Direction
    {
        Up = 90,
        Down = -90,
        Right = 360,
        Left = 180,
        UpRight = 45,
        UpLeft = 135,
        DownRight = -45,
        DownLeft = -135,
    }

    public static class DirectionExtension
    {
        public static int ToAngle(this Direction direction)
        {
            return (int) direction;
        }
    }
}