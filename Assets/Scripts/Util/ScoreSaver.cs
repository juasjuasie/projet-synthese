﻿using System;
using Harmony;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    //Author: Eduardo Breton
    [Findable(R.S.Tag.ScoreSaver)]
    public class ScoreSaver : MonoBehaviour
    {
        private ScoreRepository scoreRepository;

        private void Awake()
        {
            scoreRepository = Finder.ScoreRepository;
        }

        public void SaveScore(ScoreEntity scoreEntity, AccountEntity accountEntity)
        {
            scoreRepository.CreateScore(accountEntity, scoreEntity);
        }
    }
}