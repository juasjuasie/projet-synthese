﻿using UnityEngine;

namespace Game
{
    public class PierceableObject : MonoBehaviour
    {
        //Only use to indicate that an object does not enter in contact with projectile
    }
}