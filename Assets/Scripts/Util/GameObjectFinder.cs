﻿using Harmony;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;

namespace Game
{
    //Author: Gabriel St-Laurent-Recoura
    public static class GameObjectFinder
    {
        [NotNull] private static GameObject player;
        private static GameObject camera;
        private static GameObject gameOverCanvas;
        private static GameObject timeToBeBoldCanvas;
        private static GameObject loadingScreenCanvas;
        private static GameObject accountPanel;

        [NotNull]
        public static GameObject Player
        {
            get
            {
                if (player == null)
                {
                    player = GameObject.FindWithTag(R.S.Tag.Player);
                }
                return player;
            }  
        }

        public static GameObject Camera
        {
            get
            {
                if (camera == null)
                {
                    camera = GameObject.FindWithTag(R.S.Tag.MainCamera);
                }
                return camera;
            }  
        }

        public static GameObject TimeToBeBoldCanvas
        {
            get
            {
                if (timeToBeBoldCanvas == null)
                {
                    timeToBeBoldCanvas = GameObject.FindWithTag(R.S.Tag.TimeToBeBold);
                }
                return timeToBeBoldCanvas;
            }  
        }
        
        public static GameObject LoadingScreenCanvas
        {
            get
            {
                if (loadingScreenCanvas == null)
                {
                    loadingScreenCanvas = GameObject.FindWithTag(R.S.Tag.LoadingScreenController);
                }
                return loadingScreenCanvas;
            }  
        }
        
        public static GameObject AccountPanel
        {
            get
            {
                if (accountPanel == null)
                {
                    accountPanel = GameObject.FindWithTag(R.S.Tag.AccountPanelController);
                }
                return accountPanel;
            }  
        }
    }
}