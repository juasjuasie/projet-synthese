﻿using UnityEngine;

namespace Game
{
    public static class Layers
    {
        public static readonly LayerMask DEFAULT = LayerMask.NameToLayer("Default");
        public static readonly LayerMask SENSOR = LayerMask.NameToLayer("Sensor");
    }
}