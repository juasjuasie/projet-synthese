﻿namespace Game
{
    public interface IShotable
    {
        void OnShot();
    }
}