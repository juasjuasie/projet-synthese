﻿using System;
using UnityEngine;

namespace Game
{
    //Author : Nicolas Bisson
    public static class RaycastCalculator
    {
        private static RaycastHit2D? GetFirstUnpierceableHit
            (Vector3 spawnPointPosition, Vector3 direction, float maxLenght)
        {
            var detectedObjects = Physics2D.RaycastAll(spawnPointPosition, direction, maxLenght);

            for (int i = 0; i < detectedObjects.Length; i++)
            {
                if (detectedObjects[i].collider.GetComponentInParent<PierceableObject>() == null)
                {
                    return detectedObjects[i];
                }
            }
            return null;
        }

        public static Vector3 GetFirstUnpierceableHitLenght
            (Vector3 spawnPointPosition, Vector3 direction, float maxLenght, out RaycastHit2D? firstObjectHit)
        {
            var lenght = Vector3.one;
            firstObjectHit = GetFirstUnpierceableHit(spawnPointPosition, direction, maxLenght);

            if (firstObjectHit == null)
                lenght.x = maxLenght;
            else
                lenght.x = firstObjectHit.Value.distance;
            return lenght;
        }
        
        public static RaycastHit2D? GetFirstSpecificColliderHit
            (Vector3 spawnPointPosition, Vector3 direction, float maxLenght, Collider2D collider)
        {
            var detectedObjects = Physics2D.RaycastAll(spawnPointPosition, direction, maxLenght);

            for (int i = 0; i < detectedObjects.Length; i++)
            {
                if (detectedObjects[i].collider == collider)
                {
                    return detectedObjects[i];
                }
            }
            return null;
        }
        
        public static RaycastHit2D? GetFirstHit(Vector3 spawnPointPosition, Vector3 direction, float maxLenght)
        {
            return Physics2D.Raycast(spawnPointPosition, direction, maxLenght);
        }
    }
}