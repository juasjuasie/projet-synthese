﻿using System;
using UnityEngine;

namespace Game
{
    public class ParticleController : MonoBehaviour
    {
        [SerializeField] private ParticleSystem particles;
        
        private void OnEnable()
        {
            ActivateParticles(false);
        }

        public void ActivateParticles(bool areParticlesNeeded)
        {
            if (areParticlesNeeded)
                particles.Play();
            else
                particles.Stop();
        }

        public void ChangeParent(Transform parent, float timeToWait)
        {
            transform.parent = parent;
            Destroy(particles, timeToWait);
        }
    }
}