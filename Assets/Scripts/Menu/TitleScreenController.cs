﻿using System;
using Harmony;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace Game
{
    //Author: Eduardo Breton
    public class TitleScreenController : MonoBehaviour
    {
        private MainMenuController mainMenuController;
        private Canvas titleScreenCanvas;

        private void Awake()
        {
            titleScreenCanvas = GetComponent<Canvas>();
            titleScreenCanvas.enabled = true;
            Cursor.visible = true;
            mainMenuController = GetComponentInParent<MainMenuController>();
        }

        private void Update()
        {
            if (Input.anyKeyDown && titleScreenCanvas.enabled)
            {
                mainMenuController.SwitchCanvas(CanvasName.LOGIN);
            }
        }
    }
}