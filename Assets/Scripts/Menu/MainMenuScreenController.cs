﻿using System;
using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game
{
    //Author: Eduardo Breton
    public class MainMenuScreenController : MonoBehaviour
    {
        private List<Button> mainMenuButtons;
        private MainMenuController mainMenuController;
        private GlobalController globalController;

        private void Awake()
        {
            mainMenuButtons = GetComponentsInChildren<Button>().ToList();
            mainMenuController = GetComponentInParent<MainMenuController>();
            globalController = Finder.GlobalController;
        }

        private void OnEnable()
        {
            for (int i = 0; i < mainMenuButtons.Count; i++)
            {
                var mainMenuButton = mainMenuButtons[i];
                mainMenuButton.onClick.AddListener(() => ChangeMenu(mainMenuButton));
            }
        }

        private void OnDisable()
        {
            for (int i = 0; i < mainMenuButtons.Count; i++)
            {
                var mainMenuButton = mainMenuButtons[i];
                mainMenuButton.onClick.RemoveListener(() => ChangeMenu(mainMenuButton));
            }
        }
        
        private void ChangeMenu(Button button)
        {
            switch (button.name)
            {
                case ButtonNames.START_GAME_BUTTON:
                    GetComponent<Canvas>().enabled = false;
                    enabled = false;
                    globalController.StartAtLevel(Level.Level1);
                    break;
                
                case ButtonNames.EXIT_GAME_BUTTON:
                    //Is functional only on build
                    Application.Quit();
                    break;
                
                case ButtonNames.EXTRAS_BUTTON:
                    SwitchCanvas(CanvasName.EXTRAS);
                    break;
                
                case ButtonNames.OPTIONS_BUTTON:
                    SwitchCanvas(CanvasName.OPTIONS);
                    break;
                
                case ButtonNames.BACK_BUTTON:
                    SwitchCanvas(CanvasName.LOGIN);
                    break;
                
                case ButtonNames.LEVEL_SELECT_BUTTON:
                    SwitchCanvas(CanvasName.LEVEL_SELECT);
                    break;

                
            }
        }
        
        private void SwitchCanvas(CanvasName canvasName)
        {
            enabled = false;
            mainMenuController.SwitchCanvas(canvasName);
        }
    }
}