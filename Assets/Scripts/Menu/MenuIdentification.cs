﻿using UnityEngine;

namespace Game
{
    //Author: Eduardo Breton
    public class MenuIdentification : MonoBehaviour
    {
        [SerializeField] private CanvasName canvasName;

        public CanvasName CanvasName => canvasName;
    }
}