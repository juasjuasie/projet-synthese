﻿using System;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    //Author: Eduardo Breton
    public class AchievementMenu : MonoBehaviour
    {
        [SerializeField] private GameObject achievementLayout;
        [SerializeField] [TextArea] private string contentFormat;
        [SerializeField] private Button goBackButton;
        [SerializeField] private VerticalLayoutGroup verticalLayoutGroup;

        private ActiveAccount activeAccount;
        private MainMenuController mainMenuController;
        private PlayerHasAnAccountEventChannel playerHasAnAccountEventChannel;

        private void Awake()
        {
            activeAccount = Finder.ActiveAccount;
            mainMenuController = GetComponentInParent<MainMenuController>();
            playerHasAnAccountEventChannel = Finder.PlayerHasAnAccountEventChannel;
        } 

        private void OnEnable()
        {
            goBackButton.onClick.AddListener(ChangeMenu);
            playerHasAnAccountEventChannel.OnAccountAssigned += ShowAchievements;
        }

        private void OnDisable()
        {
            goBackButton.onClick.RemoveListener(ChangeMenu);
            playerHasAnAccountEventChannel.OnAccountAssigned -= ShowAchievements;
        }

        private void ChangeMenu()
        {
            gameObject.SetActive(false);
            mainMenuController.SwitchCanvas(CanvasName.EXTRAS);
        }

        private void ShowAchievements()
        {
            var unlockedAchievements = activeAccount.Achievements;
            if (unlockedAchievements != null)
            {
                ClearAchievements();
                foreach (var achievement in unlockedAchievements)
                {
                    var newAchievementLayout =
                        Instantiate(achievementLayout, Vector3.zero, Quaternion.identity, verticalLayoutGroup.transform);
                    string[] args = {achievement.Title, achievement.Description};
                    string layoutText = string.Format(contentFormat, args);
                    newAchievementLayout.GetComponentInChildren<TMP_Text>().text = layoutText;
                }
            }
        }

        private void ClearAchievements()
        {
            foreach (Transform child in verticalLayoutGroup.transform) 
            { 
                Destroy(child.gameObject);
            }
        }
    }
}