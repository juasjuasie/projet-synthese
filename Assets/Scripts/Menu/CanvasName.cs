﻿namespace Game
{
    //Author: Eduardo Breton
    public enum CanvasName
    {
        TITLE_SCREEN,
        LOGIN,
        MAIN_MENU_SCREEN,
        OPTIONS,
        EXTRAS,
        ACHIEVEMENT,
        SCORE,
        CREDITS,
        LEVEL_SELECT
    }
}