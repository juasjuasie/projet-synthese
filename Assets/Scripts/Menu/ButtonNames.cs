﻿namespace Game
{
    //Author: Eduardo Breton
    public static class ButtonNames
    {
        public const string START_GAME_BUTTON = "StartGameButton";
        public const string EXIT_GAME_BUTTON = "ExitButton";
        public const string OPTIONS_BUTTON = "OptionsButton";
        public const string EXTRAS_BUTTON = "ExtrasButton";
        public const string BACK_BUTTON = "BackButton";
        public const string SCORE_BUTTON = "ScoreboardButton";
        public const string ACHIEVEMENT_BUTTON = "AchievementButton";
        public const string CREDITS_BUTTON = "CreditsButton";
        public const string LEVEL_SELECT_BUTTON = "LevelSelectButton";
        public const string LEVEL1_BUTTON = "Level1Button";
        public const string LEVEL2_BUTTON = "Level2Button";
        public const string LEVEL3_BUTTON = "Level3Button";
        public const string LEVEL4_BUTTON = "Level4Button";
        public const string LEVEL5_BUTTON = "Level5Button";
    }
}