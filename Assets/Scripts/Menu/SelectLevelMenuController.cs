﻿using System.Collections.Generic;
using System.Linq;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    //Author: Eduardo Breton
    public class SelectLevelMenuController : MonoBehaviour
    {
        private List<Button> levelSelectButtons;
        private GlobalController globalController;
        private MainMenuController mainMenuController;
        private MusicController musicController;

        private void Awake()
        {
            levelSelectButtons = GetComponentsInChildren<Button>().ToList();
            globalController = Finder.GlobalController;
            mainMenuController = GetComponentInParent<MainMenuController>();
            musicController = Finder.MusicController;
        }

        private void OnEnable()
        {
            for (int i = 0; i < levelSelectButtons.Count; ++i)
            {
                var button = levelSelectButtons[i];
                button.onClick.AddListener(() => OnLevelButtonClicked(button));
            }
        }

        private void OnDisable()
        {
            for (int i = 0; i < levelSelectButtons.Count; i++)
            {
                var button = levelSelectButtons[i];
                button.onClick.RemoveListener(() => OnLevelButtonClicked(button));
            }
        }

        private void OnLevelButtonClicked(Button button)
        {
            switch (button.name)
            {
                case ButtonNames.LEVEL1_BUTTON:
                    ChangeLevel(Level.Level1);
                    break;

                case ButtonNames.LEVEL2_BUTTON:
                    ChangeLevel(Level.Level2);
                    break;

                case ButtonNames.LEVEL3_BUTTON:
                    ChangeLevel(Level.Level3);
                    break;

                case ButtonNames.LEVEL4_BUTTON:
                    ChangeLevel(Level.Level4);
                    break;

                case ButtonNames.LEVEL5_BUTTON:
                    ChangeLevel(Level.Level5);
                    break;

                case ButtonNames.BACK_BUTTON:
                    enabled = false;
                    mainMenuController.SwitchCanvas(CanvasName.MAIN_MENU_SCREEN);
                    break;
            }
        }

        private void ChangeLevel(Level levelToLoad)
        {
            enabled = false;
            GetComponent<Canvas>().enabled = false;
            musicController.SetMusicByLevel((int)levelToLoad);
            globalController.StartAtLevel(levelToLoad);
        }
    }
}