﻿using System;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    //Author: Eduardo Breton
    public class CreditsController : MonoBehaviour
    {
        private Button goBackButton;
        private MainMenuController mainMenuController;

        private void Awake()
        {
            goBackButton = GetComponentInChildren<Button>();
            mainMenuController = GetComponentInParent<MainMenuController>();
        }

        private void OnEnable()
        {
            goBackButton.onClick.AddListener(OnGoBackButtonClicked);
        }

        private void OnGoBackButtonClicked()
        {
            mainMenuController.SwitchCanvas(CanvasName.EXTRAS);
        }
    }
}