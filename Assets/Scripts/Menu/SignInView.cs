﻿using System;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    //Author: Eduardo Breton
    public class SignInView : MonoBehaviour
    {
        [SerializeField] private int newNameCharNbLimit = 12;
        
        private TMP_InputField inputField;
        private MainMenuController mainMenuController;
        private AccountRepository accountRepository;
        private ActiveAccount activeAccount;
        private Button saveButton;
        private PlayerHasAnAccountEventChannel playerHasAnAccountEventChannel;
        
        private void Awake()
        {
            inputField = GetComponentInChildren<TMP_InputField>();
            inputField.characterLimit = newNameCharNbLimit-1;
            saveButton = GetComponentInChildren<Button>();
            mainMenuController = GetComponentInParent<MainMenuController>();
            accountRepository = Finder.AccountRepository;
            activeAccount = Finder.ActiveAccount;
            playerHasAnAccountEventChannel = Finder.PlayerHasAnAccountEventChannel;
        }

        private void OnEnable()
        {
            if(inputField != null)
                inputField.onSubmit.AddListener(OnEnterSignIn);
            
            saveButton.onClick.AddListener(OnButtonSignIn);
        }

        private void OnDisable()
        {
            if(inputField != null)
                inputField.onSubmit.RemoveListener(OnEnterSignIn);
            
            saveButton.onClick.RemoveListener(OnButtonSignIn);
        }

        private void OnEnterSignIn(string username)
        {
            if (!string.IsNullOrWhiteSpace(username))
            {
               CreateNewAccount(username);
            }
        }

        private void OnButtonSignIn()
        {
            var username = inputField.text;
            if (!string.IsNullOrWhiteSpace(username))
            {
                CreateNewAccount(username);
            }
        }

        private void CreateNewAccount(string userName)
        {
            string curatedName;
            
            if (userName.Length >= newNameCharNbLimit)
                curatedName = userName.Remove(newNameCharNbLimit);
            else
                curatedName = userName;
            
            var newAccountName = new AccountEntity(curatedName);
            var newAccount = accountRepository.CreateAccount(newAccountName);
            activeAccount.SetActiveAccount(newAccount);
            mainMenuController.UpdateMenu(CanvasName.ACHIEVEMENT);
            playerHasAnAccountEventChannel.Publish();
            mainMenuController.SwitchCanvas(CanvasName.MAIN_MENU_SCREEN);
        }
    }
}