﻿using System;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    //Author: Eduardo Breton
    public class AccountView : MonoBehaviour
    {
        [SerializeField] private Button selectButton;
        [SerializeField] private Button deleteButton;
        [SerializeField] private TMP_Text accountName;
       
        private MainMenuController mainMenuController;
        private ActiveAccount activeAccount;
        private PlayerHasAnAccountEventChannel playerHasAnAccountEventChannel;
        private AccountRepository accountRepository;
        private AccountEntity saveFileAccount;

        private void Awake()
        {
            mainMenuController = GetComponentInParent<MainMenuController>();
            activeAccount = Finder.ActiveAccount;
            saveFileAccount = activeAccount.AccountEntity;
            playerHasAnAccountEventChannel = Finder.PlayerHasAnAccountEventChannel;
            accountRepository = Finder.AccountRepository;
        }

        private void OnEnable()
        {
            selectButton.onClick.AddListener(LogIn);
            deleteButton.onClick.AddListener(DeleteAccount);
        }

        private void OnDisable()
        {
            selectButton.onClick.RemoveListener(LogIn);
            deleteButton.onClick.AddListener(DeleteAccount);
        }

        private void LogIn()
        {
            activeAccount.SetActiveAccount(saveFileAccount);
            mainMenuController.UpdateMenu(CanvasName.ACHIEVEMENT);
            mainMenuController.SwitchCanvas(CanvasName.MAIN_MENU_SCREEN);
            playerHasAnAccountEventChannel.Publish();
        }

        private void DeleteAccount()
        {
            accountRepository.DeleteAccount(saveFileAccount);
            mainMenuController.SwitchCanvas(CanvasName.LOGIN);
        }

        public void AssignAccount(AccountEntity receivedAccount)
        {
            saveFileAccount = receivedAccount;
            accountName.text = saveFileAccount.Name;
        }
    }
}