﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    //Author: Eduardo Breton
    public class ExtraMenuController : MonoBehaviour
    {
        private List<Button> extrasButtons;
        private MainMenuController mainMenuController;

        private void Awake()
        {
            extrasButtons = GetComponentsInChildren<Button>().ToList();
            mainMenuController = GetComponentInParent<MainMenuController>();
        }

        private void OnEnable()
        {
            for (int i = 0; i < extrasButtons.Count; ++i)
            {
                var extraButton = extrasButtons[i];
                extraButton.onClick.AddListener(() => ChangeMenu(extraButton));
            }
        }

        private void OnDisable()
        {
            for (int i = 0; i < extrasButtons.Count; i++)
            {
                var extraButton = extrasButtons[i];
                extraButton.onClick.RemoveListener(() => ChangeMenu(extraButton));
            }
        }

        private void ChangeMenu(Button button)
        {
            switch (button.name)
            {
                case ButtonNames.SCORE_BUTTON:
                    SwitchCanvas(CanvasName.SCORE);
                    break;
                case ButtonNames.ACHIEVEMENT_BUTTON:
                    SwitchCanvas(CanvasName.ACHIEVEMENT);
                    break;
                case ButtonNames.CREDITS_BUTTON:
                    SwitchCanvas(CanvasName.CREDITS);
                    break;
                case ButtonNames.BACK_BUTTON:
                    SwitchCanvas(CanvasName.MAIN_MENU_SCREEN);
                    break;
            }
        }

        private void SwitchCanvas(CanvasName canvasName)
        {
            enabled = false;
            mainMenuController.SwitchCanvas(canvasName);
        }
    }
}