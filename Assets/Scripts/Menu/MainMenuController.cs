﻿using System;
using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Eduardo Breton
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField] private CanvasName activeCanvas = CanvasName.TITLE_SCREEN;

        private GameObject[] mainMenuCanvas;

        private void Awake()
        {
            var timeScalerController = Finder.TimeScalerController;
            mainMenuCanvas = this.Children();

            for (int i = 0; i < mainMenuCanvas.Length; i++)
            {
                mainMenuCanvas[i].GetComponent<Canvas>().enabled = false;
            }

            ShowCanvas(activeCanvas);
        }

        public void SwitchCanvas(CanvasName canvasToLoad)
        {
            HideCanvas(activeCanvas);
            ShowCanvas(canvasToLoad);
        }

        private void ShowCanvas(CanvasName canvasName)
        {
            var canvasToLoad = mainMenuCanvas.FirstOrDefault(c =>
                c.GetComponentInChildren<MenuIdentification>().CanvasName == canvasName);

            SetCanvasVisibility(canvasToLoad, true);

            activeCanvas = canvasName;
        }

        private void HideCanvas(CanvasName canvasName)
        {
            var canvasToLoad = mainMenuCanvas.FirstOrDefault(c =>
                c.GetComponentInChildren<MenuIdentification>().CanvasName == canvasName);

            SetCanvasVisibility(canvasToLoad, false);
        }

        private void SetCanvasVisibility(GameObject gameObjectCanvas, bool isVisible)
        {
            gameObjectCanvas.GetComponent<Canvas>().enabled = isVisible;
            gameObjectCanvas.SetActive(isVisible);
        }

        public void UpdateMenu(CanvasName canvasName)
        {
            var canvasToUpdate = mainMenuCanvas.FirstOrDefault(c =>
                c.GetComponentInChildren<MenuIdentification>().CanvasName == canvasName);
            
            canvasToUpdate.SetActive(false);
            canvasToUpdate.SetActive(true);
        }
    }
}