﻿using System;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    //Author: Eduardo Breton, Antony Ménard
    public class OptionsController : MonoBehaviour
    {
        private Button backButton;
        private MainMenuController mainMenuController;
        private Slider slider;
        private MusicController musicController;
        private VolumeChangedEventChannel volumeChangedEventChannel;

        private void Awake()
        {
            volumeChangedEventChannel = Finder.VolumeChangedEventChannel;
            backButton = GetComponentInChildren<Button>();
            mainMenuController = GetComponentInParent<MainMenuController>();
            musicController = Finder.MusicController;
            slider = GetComponentInChildren<Slider>();
            slider.value = musicController.GetGameVolume();
        }

        private void OnEnable()
        {
            backButton.onClick.AddListener(OnGoBackButtonClicked);
            slider.onValueChanged.AddListener(OnVolumeValueChanged);
        }

        private void OnDisable()
        {
            backButton.onClick.RemoveListener(OnGoBackButtonClicked);
            slider.onValueChanged.RemoveListener(OnVolumeValueChanged);
        }

        private void OnGoBackButtonClicked()
        {
            mainMenuController.SwitchCanvas(CanvasName.MAIN_MENU_SCREEN);
        }

        private void OnVolumeValueChanged(float value)
        {
            volumeChangedEventChannel.Publish(value);
        }
    }
}