﻿using System;
using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    //Author: Eduardo Breton
    public class AccountListView : MonoBehaviour
    {
        [SerializeField] private int maxSavefilesNumber = 3;
        [SerializeField] private GameObject saveFile;
        [SerializeField] private GameObject inputFile;

        private AccountRepository accountRepository;

        private void Awake()
        {
            accountRepository = Finder.AccountRepository;
        }

        private void OnEnable()
        {
            UpdateView();
        }

        private void OnDisable()
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }
        }

        private void UpdateView()
        {
            var accounts = accountRepository.FindAccounts();
            foreach (var account in accounts)
            {
                PlaceExistingSaveFiles(account);
            }

            var freeSpace = maxSavefilesNumber - accounts.Count;
            if (freeSpace > 0)
                PlaceEmptySaveFiles(freeSpace);
        }

        private void PlaceExistingSaveFiles(AccountEntity account)
        {
            var newSaveFile = Instantiate(saveFile, transform.position, Quaternion.identity, transform);
            var connectAccount = newSaveFile.GetComponentInChildren<AccountView>();
            connectAccount.AssignAccount(account);
        }

        private void PlaceEmptySaveFiles(int freeSpace)
        {
            for (int i = 0; i < freeSpace; i++)
            {
                Instantiate(inputFile, Vector3.zero, Quaternion.identity, transform);
            }
        }
    }
}