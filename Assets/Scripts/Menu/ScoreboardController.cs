﻿using System;
using System.Collections.Generic;
using Game;
using Harmony;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    //Author: Eduardo Breton
    public class ScoreboardController : MonoBehaviour
    {
        [SerializeField] private GameObject scoreLayout;
        [SerializeField] private string contentFormat = "{0} : {1}";
        [SerializeField] private Button goBackButton;

        private ScoreRepository scoreRepository;
        private MainMenuController mainMenuController;

        private void Awake()
        {
            scoreRepository = Finder.ScoreRepository;
            mainMenuController = GetComponentInParent<MainMenuController>();
        }

        private void Start()
        {
            var scores = FindExistingScores();
            LoadScoreBoard(scores);
        }

        private void OnEnable()
        {
            goBackButton.onClick.AddListener(ChangeMenu);
        }

        private void OnDisable()
        {
            goBackButton.onClick.RemoveListener(ChangeMenu);
        }

        private void ChangeMenu()
        {
            mainMenuController.SwitchCanvas(CanvasName.EXTRAS);
        }

        private List<ScoreEntity> FindExistingScores()
        {
            return scoreRepository.FindScores();
        }

        private void LoadScoreBoard(List<ScoreEntity> scores)
        {
            foreach (var score in scores)
            {
                var newScoreLayout = Instantiate(scoreLayout, Vector3.zero, Quaternion.identity, transform);
                string[] args = {score.Player.Name, score.Score.ToString()};
                string layoutText = string.Format(contentFormat, args);
                newScoreLayout.GetComponent<TMP_Text>().text = layoutText;
            }
        }
    }
}