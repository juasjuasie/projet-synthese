﻿using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Eduardo Breton
    [Findable(R.S.Tag.ActiveAccount)]
    public class ActiveAccount : MonoBehaviour
    {
        private AccountEntity accountEntity;
        private List<UserAchievementEntity> achievements;
        private UserAchievementRepository userAchievementRepository;

        public AccountEntity AccountEntity => accountEntity;

        public List<UserAchievementEntity> Achievements
        {
            get => achievements;
            set => achievements = value;
        }

        public void SetActiveAccount(AccountEntity accountEntity)
        {
            this.accountEntity = accountEntity;
            achievements = null;
            GetAchievements();
        }

        private void GetAchievements()
        {
            userAchievementRepository = Finder.UserAchievementRepository;
            achievements = userAchievementRepository.FindUserAchievements(accountEntity);
        }
    }
}