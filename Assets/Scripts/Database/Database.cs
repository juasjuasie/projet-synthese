﻿using System;
using System.Collections;
using System.Data;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Eduardo Breton
    [Findable(R.S.Tag.Database)]
    public class Database : MonoBehaviour
    {
        private IDbConnection databaseConnection;
        private SqLiteConnectionFactory sqLiteConnectionFactory;

        public IDbConnection DatabaseConnection => databaseConnection;

        private void Awake()
        {
            sqLiteConnectionFactory = GetComponent<SqLiteConnectionFactory>();
            DbConnect();
        }

        private void OnApplicationQuit()
        {
            DbDisconnect();
        }

        private void DbConnect()
        {
            databaseConnection = sqLiteConnectionFactory.GetConnection();
            databaseConnection.Open();
        }

        private void DbDisconnect()
        {
            //Should only close if the database is already open
            if (databaseConnection.State != ConnectionState.Closed)
                databaseConnection.Close();
        }
    }
}