﻿using System.Collections.Generic;
using Harmony;

namespace Game
{
    //Author : Nicolas Bisson
    public static class AchievementHandler
    {
        private const int SCORE_FOR_SICK_MOVES = 10000;

        private static AchievementProgressedEventChannel eventChannel = Finder.AchievementProgressedEventChannel;
        private static bool[] beBoldZoneCompleted = new bool[5] {false, false, false, false, false};

        private static GlobalController globalController => Finder.GlobalController;
        private static List<UserAchievementEntity> UnlockedAchievements => Finder.ActiveAccount.Achievements;

        public static void CheckForSpecificAchievement(Achievement achievement)
        {
            if (!HasUnlockedAchievement(achievement))
            {
                if (achievement == Achievement.NoHelp)
                {
                    beBoldZoneCompleted[(int) globalController.CurrentLevel] = true;
                }
                eventChannel.Publish(achievement);
            }
        }
        
        public static void CheckSingleLevelEndAchievement(bool hasDiedThisLevel)
        {
            if (!HasUnlockedAchievement(Achievement.EasyMode) && !hasDiedThisLevel)
                eventChannel.Publish(Achievement.EasyMode);
        }

        public static void CheckForEndGameAchievement(bool hasDied, uint score)
        {
            CheckForGameFinishedAchievement();
            CheckForNoDeathAchievement(hasDied);
            CheckForScoreAchievement(score);
            CheckForAllBeBoldZoneCompletedAchievement();
        }

        private static void CheckForGameFinishedAchievement()
        {
            if (!HasUnlockedAchievement(Achievement.Winner))
                eventChannel.Publish(Achievement.Winner);
        }

        private static void CheckForNoDeathAchievement(bool hasDied)
        {
            if (!HasUnlockedAchievement(Achievement.TheBest) && !hasDied)
                eventChannel.Publish(Achievement.TheBest);
        }

        public static void CheckForScoreAchievement(uint score)
        {
            if (!HasUnlockedAchievement(Achievement.SickMoves) && score >= SCORE_FOR_SICK_MOVES)
                eventChannel.Publish(Achievement.SickMoves);
        }

        private static void CheckForAllBeBoldZoneCompletedAchievement()
        {
            if (!HasUnlockedAchievement(Achievement.BeBold))
            {
                var hasCompletedAllBeBoldZone = true;
                for (int i = 0; i < beBoldZoneCompleted.Length; i++)
                {
                    if (HasCompletedBeBoldZone(i))
                    {
                        hasCompletedAllBeBoldZone = false;
                        break;
                    }
                }

                if (hasCompletedAllBeBoldZone)
                {
                    eventChannel.Publish(Achievement.BeBold);
                }
            }
        }

        private static bool HasCompletedBeBoldZone(int index)
        {
            return !beBoldZoneCompleted[index];
        }

        private static bool HasUnlockedAchievement(Achievement achievement)
        {
            var unlockedAchievements = UnlockedAchievements;
            if (unlockedAchievements.Count == 0)
            {
                return false;
            }
            
            foreach (var unlockedAchievement in unlockedAchievements)
            {
                if (achievement == unlockedAchievement.AchievementId)
                {
                    return true;
                }
            }
            return false;
        }
    }
}