﻿using System;
using UnityEngine;

namespace Game
{
    public class DestroyableAchievement : AchievementTrigger
    {
        private void OnDestroy()
        {
            PublishEvent();
        }
    }
}