﻿using Harmony;

namespace Game
{
    [Findable(R.S.Tag.AchievementProgressedEventChannel)]
    public class AchievementProgressedEventChannel : EventChannel<Achievement>
    {
        public event EventHandler<Achievement> OnAchievementProgressed;
        
        public override void Publish(Achievement achievement)
        {
            if (OnAchievementProgressed != null)
            {
                OnAchievementProgressed(achievement);
            } 
        }
    }
}