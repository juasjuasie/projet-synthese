﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Nicolas Bisson
    public class EliminationZoneAchievement : ClearableAchievement
    {
        [SerializeField] private GameObject destroyableInZone;
        [SerializeField] private int nbObjectsToDestroy = 10;

        private int nbObjectToDestroyInZoneAtStart;
        
        private void Start()
        {
            nbObjectToDestroyInZoneAtStart = destroyableInZone.Children().Length;
        }

        protected override void OnUnsensedPlayer(PlayerController player)
        {
            if (nbObjectToDestroyInZoneAtStart - destroyableInZone.Children().Length <= nbObjectsToDestroy)
                base.OnUnsensedPlayer(player);
        }
    }
}