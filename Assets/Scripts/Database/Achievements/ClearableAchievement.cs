﻿namespace Game
{
    public class ClearableAchievement : AchievementTrigger
    {
        private ISensor<PlayerController> sensorPlayer;

        protected virtual void Awake()
        {
            sensorPlayer = GetComponentInChildren<Sensor>().For<PlayerController>();
        }

        protected virtual void OnEnable()
        {
            sensorPlayer.OnSensedObject += OnSensedPlayer;
            sensorPlayer.OnUnsensedObject += OnUnsensedPlayer;
        }

        protected virtual void OnDisable()
        {
            sensorPlayer.OnSensedObject -= OnSensedPlayer;
            sensorPlayer.OnUnsensedObject -= OnUnsensedPlayer;
        }

        private void OnSensedPlayer(PlayerController player)
        {
            //If this method doesn't exist the OnUnsensedPlayer method will never be called
        }

        protected virtual void OnUnsensedPlayer(PlayerController player)
        {
            PublishEvent();
        }
    }
}