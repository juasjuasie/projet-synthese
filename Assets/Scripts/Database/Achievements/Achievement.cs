﻿namespace Game
{
    public enum Achievement
    {
        None = 0,
        BoldnessReward = 1,
        SickMoves = 2,
        MultipleKills = 3,
        EnergyFriendly = 4,
        Gravity = 5,
        EasyMode = 6,
        NoHelp = 7,
        BeBold = 8,
        TheBest = 9,
        Winner = 10
    }
}