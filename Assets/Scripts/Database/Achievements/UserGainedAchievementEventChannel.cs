﻿using Harmony;
using UnityEngine;

namespace Game
{
    [Findable(R.S.GameObject.UserGainedAchievementEventChannel)]
    public class UserGainedAchievementEventChannel : EventChannel
    {
        public event EventHandler OnAchGained;
        
        public override void Publish()
        {
            if (OnAchGained != null)
            {
                OnAchGained();
            }
        }
    }
}