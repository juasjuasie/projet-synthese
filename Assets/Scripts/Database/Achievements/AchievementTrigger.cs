﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    public abstract class AchievementTrigger : MonoBehaviour
    {
        [SerializeField] protected Achievement targetAchievement = Achievement.None;

        protected void PublishEvent()
        {
            if (targetAchievement != Achievement.None)
                AchievementHandler.CheckForSpecificAchievement(targetAchievement);
        }
    }
}