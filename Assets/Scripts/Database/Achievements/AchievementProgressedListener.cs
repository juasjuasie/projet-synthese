﻿﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Nicolas Bisson, Eduardo Breton
    public class AchievementProgressedListener : MonoBehaviour
    {
        private AchievementProgressedEventChannel achievementProgressedEvent;
        private UserAchievementRepository userAchievementRepository;
        private ActiveAccount activeAccount;

        private void Awake()
        {
            achievementProgressedEvent = Finder.AchievementProgressedEventChannel;
            userAchievementRepository = Finder.UserAchievementRepository;
            activeAccount = Finder.ActiveAccount;
        }

        private void OnEnable()
        {
            achievementProgressedEvent.OnAchievementProgressed += OnAchievementProgressed;
        }

        private void OnDisable()
        {
            achievementProgressedEvent.OnAchievementProgressed -= OnAchievementProgressed;
        }

        private void OnAchievementProgressed(Achievement achievement)
        {
            SaveUserAchievement(achievement);
        }

        private void SaveUserAchievement(Achievement achievement)
        {
            activeAccount.Achievements.Add(userAchievementRepository.CreateUserAchievement(activeAccount.AccountEntity, achievement));
        }
    }
}