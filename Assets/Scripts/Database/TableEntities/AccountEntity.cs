﻿namespace Game
{
    //Author: Eduardo Breton
    public class AccountEntity
    {
        public uint PlayerId { get; }
        public string Name { get; }
        
        public AccountEntity(uint playerId, string name)
        {
            PlayerId = playerId;
            Name = name;
        }
        //Only made for it to be CRUD complete.
        public AccountEntity(string name)
        {
            Name = name;
        }
    }
}