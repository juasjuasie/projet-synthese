﻿namespace Game
{
    //Author: Eduardo Breton
    public class UserAchievementEntity
    {
        public Achievement AchievementId { get; }
        public string Title { get; }
        public string Description { get; }
        //Only made for it to be CRUD complete.//For CRUD complete only
        public uint PlayerId { get; }

        public UserAchievementEntity(Achievement achievementId, string title, string description )
        {
            AchievementId = achievementId;
            Title = title;
            Description = description;
        }
        //Only made for it to be CRUD complete.
        public UserAchievementEntity(Achievement achievementId, uint playerId)
        {
            AchievementId = achievementId;
            PlayerId = playerId;
        }
        
        
    }
}