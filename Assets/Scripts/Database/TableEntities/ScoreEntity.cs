﻿using Game;

namespace Game
{
    //Author: Eduardo Breton
    public class ScoreEntity
    {
        public AccountEntity Player { get; }
        public uint Score { get; }
        //Only made for it to be CRUD complete.
        public uint ScoreId { get; }
        

        public ScoreEntity(AccountEntity player, uint score)
        {
            Player = player;
            Score = score;
        }
        
        //Only made for it to be CRUD complete.
        public ScoreEntity(uint scoreId, uint score)
        {
            ScoreId = scoreId;
            Score = score;
        }
    }
}