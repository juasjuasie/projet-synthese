﻿using System;
using System.Collections.Generic;

namespace Game
{
    public class SQLInsertAchievementCommands
    {
        private const string INSERT_ACHIEVEMENT_1 = @"INSERT INTO Achievements
            (
                Title,
                Description
            )
            VALUES
            (
                ""First reward of boldness!"" ,
                ""Pick up every energy orb at the end of the first Be Bold zone""
            )";

        private const string INSERT_ACHIEVEMENT_2 = @"INSERT INTO Achievements
            (
	            Title,
	            Description
            )
            VALUES
            (
	            ""Sick moves bro!"",
                ""Gain more than 10 000 points in a single game""
        );";

        private const string INSERT_ACHIEVEMENT_3 = @"INSERT INTO Achievements
            (
	            Title,
	            Description
            )
            VALUES
            (
	            ""Multiple shots, multiple kills"",
                ""Kill every enemies in any Be Bold zone""
            );";

        private const string INSERT_ACHIEVEMENT_4 = @"INSERT INTO Achievements
            (
	            Title,
	            Description
            )
            VALUES
            (
	            ""Energy friendly"",
                ""Collect 30 energy orbs in the second level""
            );";

        private const string INSERT_ACHIEVEMENT_5 = @"INSERT INTO Achievements
            (
	            Title,
	            Description
            )
            VALUES
            (
	            ""Gravity? Who gives a cares about gravity?"",
                ""Enter an inversed gravity zone for the first time""
            );";

        private const string INSERT_ACHIEVEMENT_6 = @"INSERT INTO Achievements
            (
	            Title,
	            Description
            )
            VALUES
            (
	            ""Is this easy mode?"",
                ""Complete any level without losing a single life""
            );";

        private const string INSERT_ACHIEVEMENT_7 = @"INSERT INTO Achievements
            (
	            Title,
	            Description
            )
            VALUES
            (
	            ""No need for help"",
                ""Complete a level other than the first without going to the easy zone ""
            );";

        private const string INSERT_ACHIEVEMENT_8 = @"INSERT INTO Achievements
            (
	            Title,
	            Description
            )
            VALUES
            (
	            ""Be Bold!"",
                ""Complete all the Be Bold zone in a single game""
            );";

        private const string INSERT_ACHIEVEMENT_9 = @"INSERT INTO Achievements
            (
	            Title,
	            Description
            )
            VALUES
            (
	            ""I'm sorry, but i'm just the best"",
                ""Complete the game without losing a single life""
            );";

        private const string INSERT_ACHIEVEMENT_10 = @"INSERT INTO Achievements
            (
	            Title,
	            Description
            )
            VALUES
            (
	            ""A winner is you"",
                ""Finish the game""
            );";

        public static string[] GetCommands()
        {
            var commands = new string[]
            {
                INSERT_ACHIEVEMENT_1,
                INSERT_ACHIEVEMENT_2,
                INSERT_ACHIEVEMENT_3,
                INSERT_ACHIEVEMENT_4,
                INSERT_ACHIEVEMENT_5,
                INSERT_ACHIEVEMENT_6,
                INSERT_ACHIEVEMENT_7,
                INSERT_ACHIEVEMENT_8,
                INSERT_ACHIEVEMENT_9,
                INSERT_ACHIEVEMENT_10
            };
            return commands;
        }
    }
}