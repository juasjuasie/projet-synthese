﻿namespace Game
{
    //Author Eduardo Breton
    public static class SQLStringParameters
    {
        public const string NAME = "@Name";
        public const string PLAYER_ID = "@PlayerId";
        public const string SCORE = "@Score";
        public const string TITLE = "@Title";
        public const string DESCRIPTION = "@Description";
        public const string ACHIEVEMENT_ID = "@AchId";
        public const string SCORE_ID = "@ScoreId";
    }
}