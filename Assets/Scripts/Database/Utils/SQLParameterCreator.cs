﻿using System.Data;

namespace Game
{
    //Author Eduardo Breton
    public static class SQLParameterCreator
    {
        public static IDbDataParameter CreateParameter(this IDbCommand command, string paramName, object value)
        {
            var parameter = command.CreateParameter();
            parameter.ParameterName = paramName;
            parameter.Value = value;
            return parameter;
        }
    }
}