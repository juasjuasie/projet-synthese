﻿namespace Game
{
    //Author Eduardo Breton
    public static class SQLConstantDbRequests
    {
        public const string INSERT_PLAYER_CMD = "INSERT INTO Player (Name) VALUES (@Name)";
        
        public const string INSERT_SCORE_CMD = "INSERT INTO Score(PlayerId, Score) VALUES(@PlayerId, @Score)";
        
        public const string INSERT_USER_ACHIEVEMENTS_CMD =
            "INSERT INTO PlayerAchievements(PlayerId, AchId) VALUES(@PlayerId,@AchId)";
        
        public const string DELETE_PLAYER_CMD = "DELETE FROM Player WHERE PlayerId = @PlayerId";
        
        public const string DELETE_SCORE_CMD = "DELETE FROM Score WHERE ScoreId = @ScoreId";

        public const string DELETE_USER_ACHIEVEMENT_CMD =
            "DELETE FROM PlayerAchievements WHERE AchId = @AchId"
            + "AND PlayerId = @PlayerId";

        public const string UPDATE_PLAYER_CMD =
            "UPDATE Player SET (Name) VALUES(@Name) WHERE PlayerId = @PlayerId";

        public const string UPDATE_SCORE_CMD =
            "UPDATE Player SET (Score, PlayerId) VALUES(@Score,@PlayerId) WHERE ScoreId = @ScoreId";

        public const string UPDATE_USER_ACHIEVEMENTS_CMD =
            "UPDATE Player SET (Name) VALUES(@Name) WHERE PlayerId = @PlayerId"
            + " AND AchId = @AchId";

        public const string SELECT_TOP20_SCORE_CMD = " SELECT Player.Name, Score FROM Score "
                                                     + " INNER JOIN Player on Player.PlayerId = Score.PlayerId"
                                                     + " ORDER BY Score DESC LIMIT 20 ";

        public const string SELECT_ALL_PLAYER_ACHIEVEMENTS_CMD =
            " SELECT Achievements.AchId, Title, Description  FROM Achievements "
            + " INNER JOIN PlayerAchievements on PlayerAchievements.AchId = Achievements.AchId"
            + " WHERE PlayerAchievements.PlayerId = @PlayerId";

        public const string SELECT_ALL_PLAYER_ACCOUNTS_CMD =
            " SELECT PlayerId, Name  FROM Player ORDER BY PlayerId ASC LIMIT 3 ";

        public const string SELECT_PLAYER_ACCOUNT_CMD =
            " SELECT PlayerId, Name  FROM Player WHERE PlayerId = @PlayerId";

        public const string SELECT_LAST_INSERT_ID_CMD = "SELECT last_insert_rowid()";
        
        public const string SELECT_USER_ACHIEVEMENT_CMD = " SELECT * FROM PlayerAchievements WHERE AchId = @AchId AND PlayerId = @PlayerId";
        
        public const string SELECT_SCORE_CMD = " SELECT ScoreId, Score FROM Score WHERE ScoreID = @ScoreId";

        public const string IS_ACHIEVEMENT_EMPTY_CMD = "SELECT count(*) FROM Achievements";
    }
}