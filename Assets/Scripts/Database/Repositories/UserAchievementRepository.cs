﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Harmony;
using JetBrains.Annotations;
using Mono.Data.Sqlite;
using UnityEngine;

namespace Game
{
    //Author Eduardo Breton
    [Findable(R.S.Tag.UserAchievementRepository)]
    public class UserAchievementRepository : MonoBehaviour
    {
        private void Start()
        {
            if (GetAchievementCount() == 0)
                CreateAchievements();
        }

        public UserAchievementEntity CreateUserAchievement(AccountEntity account, Achievement achId)
        {
            var database = Finder.Database;
            UserAchievementEntity userAchievementEntity;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                using (var transaction = database.DatabaseConnection.BeginTransaction())
                {
                    command.CommandText = SQLConstantDbRequests.INSERT_USER_ACHIEVEMENTS_CMD;
                    var playerIdParameter = command.CreateParameter(SQLStringParameters.PLAYER_ID, account.PlayerId);
                    var achIdParameter = command.CreateParameter(SQLStringParameters.ACHIEVEMENT_ID, achId);
                    command.Parameters.Add(playerIdParameter);
                    command.Parameters.Add(achIdParameter);
                    userAchievementEntity = new UserAchievementEntity((Achievement) achId, account.PlayerId);
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
            }

            return userAchievementEntity;
        }

        private void CreateAchievements()
        {
            var stringCommands = SQLInsertAchievementCommands.GetCommands();
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                using (var transaction = database.DatabaseConnection.BeginTransaction())
                {
                    foreach (var stringCommand in stringCommands)
                    {
                        command.CommandText = stringCommand;
                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
            }
        }

        public List<UserAchievementEntity> FindUserAchievements(AccountEntity account)
        {
            var userAchievements = new List<UserAchievementEntity>();

            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                command.CommandText = SQLConstantDbRequests.SELECT_ALL_PLAYER_ACHIEVEMENTS_CMD;
                var playerIdParameter = command.CreateParameter(SQLStringParameters.PLAYER_ID, account.PlayerId);
                command.Parameters.Add(playerIdParameter);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        userAchievements.Add(new UserAchievementEntity(
                            (Achievement) reader.GetInt32(0),
                            reader.GetString(1),
                            reader.GetString(2)));
                    }
                }
            }

            return userAchievements;
        }

        //For CRUD complete only
        public UserAchievementEntity FindUserAchievement(uint playerId, Achievement achievementId)
        {
            UserAchievementEntity userAchievementEntity = null;
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                command.CommandText = SQLConstantDbRequests.SELECT_USER_ACHIEVEMENT_CMD;
                var playerIdParameter = command.CreateParameter(SQLStringParameters.PLAYER_ID, playerId);
                var playerIdAchievement = command.CreateParameter(SQLStringParameters.ACHIEVEMENT_ID, achievementId);
                command.Parameters.Add(playerIdParameter);
                command.Parameters.Add(playerIdAchievement);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        userAchievementEntity = new UserAchievementEntity((Achievement) reader.GetInt32(0),
                            (uint) reader.GetInt32(1));
                    }
                }
            }

            return userAchievementEntity;
        }

        private int GetAchievementCount()
        {
            var database = Finder.Database;
            long numberOfAchievements = 0;

            using (var command = database.DatabaseConnection.CreateCommand())
            {
                command.CommandText = SQLConstantDbRequests.IS_ACHIEVEMENT_EMPTY_CMD;
                numberOfAchievements = (long) command.ExecuteScalar();
            }

            return Convert.ToInt32(numberOfAchievements);
        }

        //For CRUD complete only
        private void UpdateUserAchievement(UserAchievementEntity userAchievementToUpdate)
        {
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                using (var transaction = database.DatabaseConnection.BeginTransaction())
                {
                    command.CommandText = SQLConstantDbRequests.UPDATE_USER_ACHIEVEMENTS_CMD;
                    var titleParameter =
                        command.CreateParameter(SQLStringParameters.TITLE, userAchievementToUpdate.Title);
                    var descriptionParameter = command.CreateParameter(SQLStringParameters.DESCRIPTION,
                        userAchievementToUpdate.Description);
                    var achievementIdParameter = command.CreateParameter(SQLStringParameters.ACHIEVEMENT_ID,
                        userAchievementToUpdate.AchievementId);
                    command.Parameters.Add(titleParameter);
                    command.Parameters.Add(descriptionParameter);
                    command.Parameters.Add(achievementIdParameter);
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
            }
        }

        //For CRUD complete only
        private void DeleteUserAchievement(UserAchievementEntity userAchievementToDelete)
        {
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                using (var transaction = database.DatabaseConnection.BeginTransaction())
                {
                    command.CommandText = SQLConstantDbRequests.DELETE_USER_ACHIEVEMENT_CMD;
                    var achievementIdParameter = command.CreateParameter(SQLStringParameters.ACHIEVEMENT_ID,
                        userAchievementToDelete.AchievementId);
                    var playerIdParameter =
                        command.CreateParameter(SQLStringParameters.PLAYER_ID, userAchievementToDelete.PlayerId);
                    command.Parameters.Add(achievementIdParameter);
                    command.Parameters.Add(playerIdParameter);
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
            }
        }
    }
}