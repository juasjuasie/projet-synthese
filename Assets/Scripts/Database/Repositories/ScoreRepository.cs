﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Harmony;
using JetBrains.Annotations;
using Mono.Data.Sqlite;
using UnityEngine;

namespace Game
{
    //Author Eduardo Breton
    [Findable(R.S.GameObject.ScoreRepository)]
    public class ScoreRepository : MonoBehaviour
    {
        public void CreateScore(AccountEntity account, ScoreEntity score)
        {
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                using (var transaction = database.DatabaseConnection.BeginTransaction())
                {
                    command.CommandText = SQLConstantDbRequests.INSERT_SCORE_CMD;
                    var playerIdParameter = command.CreateParameter(SQLStringParameters.PLAYER_ID, account.PlayerId);
                    var scoreParameter = command.CreateParameter(SQLStringParameters.SCORE, score.Score);
                    command.Parameters.Add(playerIdParameter);
                    command.Parameters.Add(scoreParameter);
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
            }
        }

        public List<ScoreEntity> FindScores()
        {
            var scoreContents = new List<ScoreEntity>();
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                command.CommandText = SQLConstantDbRequests.SELECT_TOP20_SCORE_CMD;
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var player = new AccountEntity(reader.GetString(0));
                        scoreContents.Add(new ScoreEntity(player, (uint) reader.GetInt32(1)));
                    }
                }
            }

            return scoreContents;
        }

        //For repo to be CRUD complete
        public ScoreEntity FindScore(uint id)
        {
            ScoreEntity scoreEntity = null;
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                command.CommandText = SQLConstantDbRequests.SELECT_SCORE_CMD;
                var parameterId = command.CreateParameter(SQLStringParameters.SCORE_ID, id);
                command.Parameters.Add(parameterId);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        scoreEntity = new ScoreEntity((uint) reader.GetInt32(0), (uint) reader.GetInt32(1));
                    }
                }
            }

            return scoreEntity;
        }

        //For repo to be CRUD complete
        public void UpdateScore(ScoreEntity scoreToUpdate)
        {
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                using (var transaction = database.DatabaseConnection.BeginTransaction())
                {
                    command.CommandText = SQLConstantDbRequests.UPDATE_SCORE_CMD;
                    var scoreParameter = command.CreateParameter(SQLStringParameters.SCORE, scoreToUpdate.Score);
                    var playerIdParameter =
                        command.CreateParameter(SQLStringParameters.PLAYER_ID, scoreToUpdate.Player.PlayerId);
                    var scoreIdParameter =
                        command.CreateParameter(SQLStringParameters.DESCRIPTION, scoreToUpdate.ScoreId);
                    command.Parameters.Add(scoreParameter);
                    command.Parameters.Add(playerIdParameter);
                    command.Parameters.Add(scoreIdParameter);
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
            }
        }

        //For repo to be CRUD complete
        public void DeleteScore(ScoreEntity scoreToDelete)
        {
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                using (var transaction = database.DatabaseConnection.BeginTransaction())
                {
                    command.CommandText = SQLConstantDbRequests.DELETE_SCORE_CMD;
                    var parameterId = command.CreateParameter(SQLStringParameters.SCORE_ID, scoreToDelete.ScoreId);
                    command.Parameters.Add(parameterId);
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
            }
        }
    }
}