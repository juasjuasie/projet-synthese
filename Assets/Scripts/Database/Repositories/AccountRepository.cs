﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using DG.Tweening;
using Harmony;
using JetBrains.Annotations;
using Mono.Data.Sqlite;
using UnityEngine;
using SqliteParameter = Mono.Data.SqliteClient.SqliteParameter;


namespace Game
{
    //Author: Eduardo Breton
    [Findable(R.S.Tag.AccountRepository)]
    public class AccountRepository : MonoBehaviour
    {
        public AccountEntity CreateAccount(AccountEntity newAccount)
        {
            AccountEntity newAccountEntity = null;
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                using (var transaction = database.DatabaseConnection.BeginTransaction())
                {
                    command.CommandText = SQLConstantDbRequests.INSERT_PLAYER_CMD;
                    var parameter = command.CreateParameter(SQLStringParameters.NAME, newAccount.Name);
                    command.Parameters.Add(parameter);
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }

                newAccountEntity = new AccountEntity(GetLastInsertId(), newAccount.Name);
            }

            return newAccountEntity;
        }


        private uint GetLastInsertId()
        {
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                command.CommandText = SQLConstantDbRequests.SELECT_LAST_INSERT_ID_CMD;
                using (var reader = command.ExecuteReader())
                {
                    var result = reader[0];
                    return (uint) Convert.ToInt32(result);
                }
            }
        }

        public List<AccountEntity> FindAccounts()
        {
            var accountList = new List<AccountEntity>();
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                command.CommandText = SQLConstantDbRequests.SELECT_ALL_PLAYER_ACCOUNTS_CMD;
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        accountList.Add(new AccountEntity((uint) reader.GetInt32(0), reader.GetString(1)));
                    }
                }
            }

            return accountList;
        }

        //For repo to be CRUD complete
        public AccountEntity FindAccount(uint id)
        {
            AccountEntity account = null;
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                command.CommandText = SQLConstantDbRequests.SELECT_PLAYER_ACCOUNT_CMD;
                var idParameter = command.CreateParameter(SQLStringParameters.PLAYER_ID, id);
                command.Parameters.Add(idParameter);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        account = new AccountEntity((uint) reader.GetInt32(0), reader.GetString(1));
                    }
                }
            }

            return account;
        }

        //For repo to be CRUD complete
        public void UpdateAccount(AccountEntity accountToUpdate)
        {
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                using (var transaction = database.DatabaseConnection.BeginTransaction())
                {
                    command.CommandText = SQLConstantDbRequests.UPDATE_PLAYER_CMD;
                    var nameParameter = command.CreateParameter(SQLStringParameters.NAME, accountToUpdate.Name);
                    var idParameter = command.CreateParameter(SQLStringParameters.PLAYER_ID, accountToUpdate.PlayerId);
                    command.Parameters.Add(nameParameter);
                    command.Parameters.Add(idParameter);
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
            }
        }

        //For repo to be CRUD complete
        public void DeleteAccount(AccountEntity accountToDelete)
        {
            var database = Finder.Database;
            using (var command = database.DatabaseConnection.CreateCommand())
            {
                using (var transaction = database.DatabaseConnection.BeginTransaction())
                {
                    command.CommandText = SQLConstantDbRequests.DELETE_PLAYER_CMD;
                    var IdParameter = command.CreateParameter(SQLStringParameters.PLAYER_ID, accountToDelete.PlayerId);
                    command.Parameters.Add(IdParameter);
                    command.ExecuteNonQuery();
                    transaction.Commit();
                }
            }
        }
    }
}