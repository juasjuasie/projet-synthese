﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Eduardo Breton
    [Findable(R.S.GameObject.PlayerGainedScoreEventChannel)]
    public class PlayerGainedScoreEventChannel : EventChannel
    {
        public event EventHandler OnScoreGained;
        public override void Publish()
        {
            if (OnScoreGained != null)
            {
                OnScoreGained();
            } 
        }
    }
}