﻿using Harmony;

namespace Game
{
    //Author: Gabriel St-Laurent-Recoura
    [Findable(R.S.Tag.PlayerDeathEventChannel)]
    public class PlayerDeathEventChannel : EventChannel
    {
        public event EventHandler OnPlayerDeath;
        public override void Publish()
        {
            if (OnPlayerDeath != null)
            {
                OnPlayerDeath();
            } 
        }
    }
}