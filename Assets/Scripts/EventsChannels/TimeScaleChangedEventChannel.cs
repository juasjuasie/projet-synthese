﻿
using Harmony;

namespace Game
{
    //Author: Gabriel St-Laurent-Recoura
    [Findable(R.S.Tag.TimeScaleChangedEventChannel)]
    public class TimeScaleChangedEventChannel : EventChannel<TimeScale>
    {
        public event EventHandler<TimeScale> OnTimeScaleChanged;
        public override void Publish(TimeScale newTimeScale)
        {
            if (OnTimeScaleChanged != null)
            {
                OnTimeScaleChanged(newTimeScale);
            } 
        }
    }
}