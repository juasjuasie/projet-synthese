﻿using Harmony;

namespace Game
{
    //Author: Eduardo Breton
    [Findable(R.S.GameObject.PlayerHasAnAccountEventChannel)]
    public class PlayerHasAnAccountEventChannel : EventChannel
    {
        public event EventHandler OnAccountAssigned;
        public override void Publish()
        {
            if (OnAccountAssigned != null)
                OnAccountAssigned();
        }
    }
}