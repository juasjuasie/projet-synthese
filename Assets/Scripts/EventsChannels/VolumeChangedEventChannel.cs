﻿using Harmony;

namespace Game
{
    //Author: Antony Ménard
    [Findable(R.S.GameObject.VolumeChangedEventChannel)]
    public class VolumeChangedEventChannel : EventChannel<float>
    {
        public event EventHandler<float> OnVolumeChanged;
        
        public override void Publish(float volume)
        {
            if (OnVolumeChanged != null)
            {
                OnVolumeChanged(volume);
            } 
        }
    }
}