﻿using UnityEngine;

namespace Game
{
    //Author: Nicolas Bisson
    public class JumpPad : MonoBehaviour
    {
        [SerializeField] private float bounceForce = 1000;
        [SerializeField][Range(0, 100)] protected float particleShowTime = 0f;
        [SerializeField] private JumpPadSfxHandler sfxHandler;
        
        private ParticleController particleController;
        private ISensor<PlayerController> sensorPlayer;

        private void Awake()
        {
            particleController = GetComponentInChildren<ParticleController>();
            sensorPlayer = GetComponentInChildren<Sensor>().For<PlayerController>();
        }

        private void OnEnable()
        {
            sensorPlayer.OnSensedObject += OnPlayerSensed;
        }

        private void OnDisable()
        {
            sensorPlayer.OnSensedObject -= OnPlayerSensed;
        }

        private void OnPlayerSensed(PlayerController player)
        {
            particleController.ActivateParticles(true);
            particleController.ChangeParent(transform.parent, particleShowTime);
            player.UseJumpPad(bounceForce);
            sfxHandler.PlayBounceSfx();
        }
    }
}