﻿using System.Collections;
using Harmony;
using SpriteGlow;
using UnityEngine;

namespace Game
{
    //Author: Eduardo Breton
    public class DoorButton : MonoBehaviour, IShotable
    {
        [SerializeField] private Door door;
        [SerializeField] private Color closedColor = Color.red;
        [SerializeField] private Color openColor = Color.green;
        [SerializeField] private DoorSystemSfxHandler sfxHandler;

        private SpriteRenderer sprite;
        private ISensor<PlayerController> sensorPlayer;
        private ISensor<EnemyProjectile> sensorProjectile;
        private SpriteGlowEffect spriteGlowEffect;

        private void Awake()
        {
            sensorPlayer = GetComponentInChildren<Sensor>().For<PlayerController>();
            sensorProjectile = GetComponentInChildren<Sensor>().For<EnemyProjectile>();
            spriteGlowEffect = GetComponentInChildren<SpriteGlowEffect>();
            sprite = GetComponentInChildren<SpriteRenderer>();
            ChangeColorToClosed();
        }

        private void OnEnable()
        {
            sensorPlayer.OnSensedObject += OnPlayerSensed;
            sensorProjectile.OnSensedObject += OnProjectileSensed;
        }

        private void OnDisable()
        {
            sensorPlayer.OnSensedObject -= OnPlayerSensed;
            sensorProjectile.OnSensedObject -= OnProjectileSensed;
        }

        private void OnProjectileSensed(EnemyProjectile projectile)
        {
            ActivateDoor();
        }

        private void OnPlayerSensed(PlayerController player)
        {
            ActivateDoor();
        }

        private void ActivateDoor()
        {
            ActivateButton();
            door.MoveDoor();
        }

        public void ChangeColorToClosed()
        {
            sprite.color = closedColor;
            spriteGlowEffect.GlowColor = closedColor;
        }

        public void ChangeColorToOpen()
        {
            sprite.color = openColor;
            spriteGlowEffect.GlowColor = openColor;
        }

        private void ActivateButton()
        {
            sfxHandler.PlayButtonTriggeredSfx();
        }

        public void OnShot()
        {
            ActivateDoor();
        }
    }
}