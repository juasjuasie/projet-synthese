﻿using System;
using UnityEngine;

namespace Game
{
    //Author : Nicolas Bisson
    public class Rail : MonoBehaviour
    {
        private ISensor<PlayerController> sensorPlayer;

        private void Awake()
        {
            sensorPlayer = GetComponentInChildren<Sensor>().For<PlayerController>();
        }

        private void OnEnable()
        {
            sensorPlayer.OnSensedObject += OnPlayerSensed;
            sensorPlayer.OnUnsensedObject += OnPlayerUnsensed;
        }

        private void OnDisable()
        {
            sensorPlayer.OnSensedObject -= OnPlayerSensed;
            sensorPlayer.OnUnsensedObject -= OnPlayerUnsensed;
        }

        private void OnPlayerSensed(PlayerController player)
        {
            player.RailConnector.TryConnectToRail(this);
        }

        private void OnPlayerUnsensed(PlayerController player)
        {
            player.RailConnector.DisconnectFromRail();
        }
    }
}