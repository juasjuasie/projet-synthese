﻿namespace Game
{
    //Author : Nicolas Bisson
    public class RailConnector
    {
        private PlayerController player;
        private PlayerMover playerMover;
        private int nbRailsInPlayerHitbox;
        private Rail connectedRail;

        public RailConnector(PlayerController player, PlayerMover playerMover)
        {
            this.player = player;
            this.playerMover = playerMover;
            nbRailsInPlayerHitbox = 0;
        }
        
        public void TryConnectToRail(Rail rail)
        {
            nbRailsInPlayerHitbox++;
            if (CanConnectToRail(rail))
            {
                ConnectToRail(rail);
            }
        }

        private bool CanConnectToRail(Rail rail)
        {
            return CanConnectToRailWhileNotOnRail() || CanConnectToRailWhileOnRail(rail);
        }

        private bool CanConnectToRailWhileNotOnRail()
        {
            return connectedRail == null && playerMover.IsFalling();
        }

        private bool CanConnectToRailWhileOnRail(Rail rail)
        {
            //Verify if the player is already on a rail, that the rail it's trying to connect to is not the one it's already on
            //and that the next rail is farther in the level than the actual one
            return connectedRail != null && connectedRail != rail && connectedRail.transform.position.x < rail.transform.position.x;
        }

        private void ConnectToRail(Rail rail)
        {
            connectedRail = rail;
            playerMover.SetPlayerToOnRailState(connectedRail);
            player.OnRailConnection();
        }

        public void DisconnectFromRail()
        {
            nbRailsInPlayerHitbox--;
            if (nbRailsInPlayerHitbox == 0 && connectedRail != null)
            {
                connectedRail = null;
                playerMover.StopPlayerOnRailState();
                player.OnRailDisconnection();
            }
        }
    }
}