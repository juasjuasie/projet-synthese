﻿using System;
using System.Collections;
using DG.Tweening;
using Harmony;
using UnityEngine;

namespace Game
{
    public class Door : MonoBehaviour
    {
        [SerializeField] private DoorButton doorButton;
        [SerializeField] private SpriteMask mask;
        [SerializeField] private float doorTimeToTransition = .2f;
        [SerializeField] private float cameraShakeForce =  0.5f;
        [SerializeField] private float cameraShakeDuration = 0.2f;
        [SerializeField] private bool isDoorOpen = false;
        [SerializeField] private DoorSystemSfxHandler sfxHandler;
        
        private Vector3 openDoorPosition;
        private Vector3 closedDoorPosition;
        private CameraShaker cameraShaker;

        private void Awake()
        {
            cameraShaker = Finder.CameraShaker;
            openDoorPosition = mask.transform.position;
            closedDoorPosition = transform.position;
        }

        private void Start()
        {
            if (isDoorOpen)
            {
                isDoorOpen = false;
                doorButton.ChangeColorToOpen();
                transform.position = openDoorPosition;
            }
        }

        public void MoveDoor()
        {
            sfxHandler.PlayDoorMovingSfx();
            
            if(!isDoorOpen)
                transform.DOMove(openDoorPosition, doorTimeToTransition)
                    .OnComplete(ChangeButtonColor);
            else
                transform.DOMove(closedDoorPosition, doorTimeToTransition)
                    .OnComplete(ChangeButtonColor);
            
            cameraShaker.ShakeCamera(cameraShakeForce, cameraShakeDuration, transform.position);
        }

        private void ChangeButtonColor()
        {
            if (!isDoorOpen)
            {
                isDoorOpen = true;
                doorButton.ChangeColorToOpen();
            }
            else
            {
                isDoorOpen = false;
                doorButton.ChangeColorToClosed();
            }
        }

        private void OnDestroy()
        {
            transform.DOKill();
        }
    }
}