﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Game;
using Harmony;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UIElements;

namespace Game
{
    //Author : Gabriel St-Laurent-Recoura
    [Findable(R.S.Tag.GlobalController)]
    public class GlobalController : MonoBehaviour
    {
        [SerializeField] private GlobalState initialGlobalState;
        [SerializeField] private SceneReference sceneGame;
        [SerializeField] private SceneReference sceneMainMenu;
        [SerializeField] private List<SceneReference> sceneLevels;

        private LoadingScreenController loadingScreenController;
        private MusicController musicController;
        private TimeScalerController timeScaler;
        private CountdownController countdownController;
        private VolumeChangedEventChannel volumeChangedEventChannel;
        private GlobalState currentGlobalState;
        private Level currentLevel;
        private float masterVolume;

        private string CurrentLevelName => sceneLevels[(int) currentLevel];
        private string NextLevelName => sceneLevels[(int) currentLevel + 1];
        
        public Level CurrentLevel => currentLevel;
        
        private void Awake()
        {
            volumeChangedEventChannel = Finder.VolumeChangedEventChannel;
            currentGlobalState = GlobalState.None;
            timeScaler = Finder.TimeScalerController;
            musicController = Finder.MusicController;
            loadingScreenController = Finder.LoadingScreenController;
        }

        private void Start()
        {
            masterVolume = musicController.GetGameVolume();
            if(initialGlobalState == GlobalState.Menu)
                GoToMenu();
            if(initialGlobalState == GlobalState.Game)
                StartAtLevel(Level.Level1);
        }

        private void OnEnable()
        {
            volumeChangedEventChannel.OnVolumeChanged += OnVolumeChanged;
        }
        
        private void OnDisable()
        {
            volumeChangedEventChannel.OnVolumeChanged -= OnVolumeChanged;
        }

        private void OnVolumeChanged(float volume)
        {
            masterVolume = musicController.GetGameVolume();
        }

        public float GetMasterVolume()
        {
            return masterVolume;
        }
        
        private void ChangeCurrentGlobalState(GlobalState newGlobalState)
        {
            RemoveAllOtherScenes();
            currentGlobalState = newGlobalState;
        }
    
        private void RemoveAllOtherScenes()
        {
            if (currentGlobalState == GlobalState.Game)
            {
                RemoveScene(sceneGame);
                RemoveScene(CurrentLevelName);
            }
            else if (currentGlobalState == GlobalState.Menu)
            {
                RemoveScene(sceneMainMenu);
            }
        }
    
        public void StartAtLevel(Level levelToStartAt)
        {
            timeScaler.ActivateTimeScale(TimeScale.Stopped);
            if(currentGlobalState == GlobalState.Menu)
                ChangeCurrentGlobalState(GlobalState.Game);
            AddScene(sceneGame.Name);
            StartCoroutine(loadingScreenController.WaitForLoadingScreen());
            AddScene(sceneLevels[(int) levelToStartAt]);
            currentLevel = levelToStartAt;
        }

        public void ChangeToNextLevel()
        {
            if (currentGlobalState == GlobalState.Game)
            {
                StartCoroutine(loadingScreenController.WaitForLoadingScreen());
                ReplaceScene(CurrentLevelName,NextLevelName);
                currentLevel++;
            }
        }

        public void GoToMenu()
        {
            timeScaler.DeactivateTimeScale(TimeScale.Stopped);
            ChangeCurrentGlobalState(GlobalState.Menu);
            AddScene(sceneMainMenu.Name);
            musicController.BackToMenuMusic();
        }

        public bool IsOnLastLevel()
        {
            return (int) currentLevel + 1 >= sceneLevels.Count;
        }
        
        public void ReloadCurrentLevel()
        {
            if (currentGlobalState == GlobalState.Game)
            {
                RestartScene(CurrentLevelName);
                musicController.ResetToLastSavedTime();
            }
        }

        private IEnumerator UnloadScene(string sceneToUnload)
        {
            yield return SceneManager.UnloadSceneAsync(sceneToUnload);
        }
    
        private IEnumerator LoadScene(string sceneToLoad)
        {
            yield return SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneToLoad));
        }

        private IEnumerator ReloadScene(string sceneToReload)
        {
            yield return StartCoroutine(UnloadScene(sceneToReload));
            yield return StartCoroutine(LoadScene(sceneToReload));
        }

        private IEnumerator ChangeScene(string sceneBeingReplaced, string sceneToReplaceWith)
        {
            yield return StartCoroutine(UnloadScene(sceneBeingReplaced));
            yield return StartCoroutine(LoadScene(sceneToReplaceWith));
        }
        
        private void RemoveScene(string sceneToRemove)
        {
            StartCoroutine(UnloadScene(sceneToRemove));
        }
    
        private void RestartScene(string sceneToRestart)
        {
            StartCoroutine(ReloadScene(sceneToRestart));
            timeScaler.DeactivateTimeScale(TimeScale.Stopped);
        }
    
        private void AddScene(string sceneToAdd)
        {
            StartCoroutine(LoadScene(sceneToAdd));
        }
        
        private void ReplaceScene(string sceneBeingReplaced, string sceneToReplaceWith)
        {
            StartCoroutine(ChangeScene(sceneBeingReplaced, sceneToReplaceWith));
        }
    }
}
