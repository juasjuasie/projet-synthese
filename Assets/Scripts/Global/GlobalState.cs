﻿namespace Game
{
    //Author : Gabriel St-Laurent-Recoura
    public enum GlobalState
    {
        Menu,
        Game,
        None,
    }
}