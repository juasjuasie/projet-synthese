﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Gabriel St-Laurent-Recoura
    [Findable(R.S.Tag.TimeScalerController)]
    public class TimeScalerController : MonoBehaviour
    {
        [SerializeField] [Range(0, 1)] public float slowingTimePercentage = 0;

        public const float NORMAL_TIME_SCALE = 1;
        public const float STOPPED_TIME_SCALE = 0;
        
        private TimeScale currentTimeScale;

        private TimeScaleChangedEventChannel timeScaleChangedEventChannel;
        private bool[] isTimeScaleActiveArray;

        private void Awake()
        {
            timeScaleChangedEventChannel = Finder.TimeScaleChangedEventChannel;
            isTimeScaleActiveArray = new bool[Enum.GetNames(typeof(TimeScale)).Length];
            for (int i = 0; i < isTimeScaleActiveArray.Length; i++)
            {
                isTimeScaleActiveArray[i] = false;
            }

            isTimeScaleActiveArray[(int) TimeScale.Normal] = true;
        }

        private TimeScale GetNewCurrentTimeScale()
        {
            for (int i = isTimeScaleActiveArray.Length - 1; 0 < i; i--)
            {
                if (isTimeScaleActiveArray[i])
                {
                    return (TimeScale) i;
                }
            }

            return TimeScale.Normal;
        }

        private void CheckForNewTimeScale()
        {
            TimeScale newCurrentTimeScale = GetNewCurrentTimeScale();
            if (currentTimeScale != newCurrentTimeScale)
            {
                currentTimeScale = newCurrentTimeScale;
                ChangeTimeScale(currentTimeScale);
                timeScaleChangedEventChannel.Publish(newCurrentTimeScale);
            }
        }

        private void ChangeTimeScale(TimeScale newTimeScale)
        {
            switch (newTimeScale)
            {
                case TimeScale.Normal:
                {
                    Time.timeScale = NORMAL_TIME_SCALE;
                    break;
                }
                case TimeScale.Slowed:
                {
                    Time.timeScale = NORMAL_TIME_SCALE * slowingTimePercentage;
                    break;
                }
                case TimeScale.Suspended:
                {
                    Time.timeScale = STOPPED_TIME_SCALE;
                    break;
                }
                case TimeScale.Frozen:
                {
                    Time.timeScale = STOPPED_TIME_SCALE;
                    break;
                }
                case TimeScale.Stopped:
                {
                    Time.timeScale = STOPPED_TIME_SCALE;
                    break;
                }
            }
        }

        public void ActivateTimeScale(TimeScale newTimeScale)
        {
            if (!isTimeScaleActiveArray[(int) newTimeScale])
            {
                isTimeScaleActiveArray[(int) newTimeScale] = true;
                CheckForNewTimeScale();
            }
        }

        public void DeactivateTimeScale(TimeScale newTimeScale)
        {
            if (isTimeScaleActiveArray[(int) newTimeScale])
            {
                isTimeScaleActiveArray[(int) newTimeScale] = false;
                CheckForNewTimeScale();
            }
        }
    }
}