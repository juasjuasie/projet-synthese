﻿namespace Game
{
    //Author : Gabriel St-Laurent-Recoura
    public enum TimeScale
    {
        Normal,
        Slowed,
        Suspended,
        Frozen,
        Stopped
    }
}