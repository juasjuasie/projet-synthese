﻿using System;
using UnityEngine;

namespace Game
{
    //Author: Antony Ménard (Adapté du code de Benjamin Lemelin)
    public sealed class Stimuli : MonoBehaviour
    {
        public event Stimuli2EventHandler OnDestroyed;

        private void Awake()
        {
            SetSensorLayer();
        }

        private void OnDestroy()
        {
            NotifyDestroyed();
        }

        private void SetSensorLayer()
        {
            gameObject.layer = Layers.SENSOR;
        }

        private void NotifyDestroyed()
        {
            if (OnDestroyed != null) OnDestroyed(transform.parent.gameObject);
        }
    }

    public delegate void Stimuli2EventHandler(GameObject otherObject);
}