﻿using System;
using DG.Tweening;
using Harmony;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace Game
{
    //Author: Nicolas Bisson, Gabriel St-Laurent-Recoura
    [Findable(R.S.Tag.CameraMover)]
    public class CameraMover : MonoBehaviour
    {
        [SerializeField] private Vector3 cameraOffSet = new Vector3(10,6,-10);
        [SerializeField] private float cameraDeadZoneDistanceUp = 6f;
        [SerializeField] private float cameraDeadZoneDistanceDown = 1f;
        [SerializeField] private float screenEdgeMargin = 1.5f;
        [SerializeField] private Camera gameCamera;

        const float HARD_SCROLL_OFFSET = 0.1f;
        
        private GameObject player;
        private GameController gameController;
        private CameraModes currentCameraMode;
        private float targetHeight;
        private float smoothScrollingSpeed;
        private float screenHeightInWorldUnits;
        private Vector3 screenEdgeMarginVector3;

        private void Awake()
        {
            screenHeightInWorldUnits = Camera.main.ScreenToWorldPoint(new Vector3(0,Camera.main.scaledPixelHeight)).y - 
                                       Camera.main.ScreenToWorldPoint(new Vector3(0,Camera.main.scaledPixelHeight / 2)).y;
            screenEdgeMarginVector3 = new Vector3(0, screenEdgeMargin, 0);
        }

        private void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void Update()
        {
            if (player != null)
            {
                MoveCamera();
            }
        }

        public void ChangeCameraMode(CameraModes newCameraMode, float newTargetHeight, float newSmoothScrollingSpeed)
        {
            currentCameraMode = newCameraMode;
            targetHeight = newTargetHeight;
            smoothScrollingSpeed = newSmoothScrollingSpeed;
            transform.DOKill();
            if (currentCameraMode == CameraModes.FollowingXOnly)
            {
                SmoothScrollTo(targetHeight);  
            }
        }

        private void MoveCamera()
        {
            var playerPosition = player.transform.position;
            var cameraPosition = transform.position;

            FollowInX(cameraPosition.y);

            switch (currentCameraMode)
            {
                case CameraModes.FollowingXOnly:
                {
                    break;
                }
                case CameraModes.FollowingXAndYBottom:
                {
                    if (!KeepPlayerInScreen(playerPosition))
                    {
                        if (playerPosition.y - cameraDeadZoneDistanceUp > cameraPosition.y - cameraOffSet.y)
                        {
                           SmoothScrollTo(playerPosition.y - cameraDeadZoneDistanceUp + cameraOffSet.y);
                        }
                        else if(playerPosition.y + cameraDeadZoneDistanceDown < cameraPosition.y - cameraOffSet.y)
                        {
                           SmoothScrollTo(playerPosition.y+ cameraDeadZoneDistanceDown + cameraOffSet.y);
                        } 
                    }
                    break;
                }
                case CameraModes.FollowingXAndYTop:
                {
                    if (!KeepPlayerInScreen(playerPosition))
                    {
                        if (playerPosition.y - cameraDeadZoneDistanceDown > cameraPosition.y + cameraOffSet.y)
                        {
                            SmoothScrollTo(playerPosition.y - cameraDeadZoneDistanceDown - cameraOffSet.y);
                        }
                        else if(playerPosition.y+ cameraDeadZoneDistanceUp < cameraPosition.y + cameraOffSet.y)
                        {
                            SmoothScrollTo(playerPosition.y + cameraDeadZoneDistanceUp - cameraOffSet.y);
                        }
                    }
                    break;
                }
            }
        }

        private bool KeepPlayerInScreen(Vector3 playerPosition)
        {
            Vector3 pos = gameCamera.WorldToViewportPoint(playerPosition - screenEdgeMarginVector3);
            
            if (pos.y < 0.0)
            {
                HardScrollTo(playerPosition.y + screenHeightInWorldUnits - screenEdgeMargin - HARD_SCROLL_OFFSET);
                return true;
            }

            pos = gameCamera.WorldToViewportPoint(player.transform.position + screenEdgeMarginVector3);
            if (1.0 < pos.y)
            {
                HardScrollTo(playerPosition.y - screenHeightInWorldUnits + screenEdgeMargin + HARD_SCROLL_OFFSET);
                return true;
            }
            return false;
        }

        private void FollowInX(float cameraY)
        {
            var tempPos = new Vector3(player.transform.position.x + cameraOffSet.x,
                cameraY,
                cameraOffSet.z);

            transform.position = tempPos;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (GameObjectFinder.Player != null)
            {
                player = GameObjectFinder.Player;
                Vector3 tempPos = player.transform.position;
                tempPos.y += cameraOffSet.y;
                tempPos.x += cameraOffSet.x;
                transform.position = tempPos;
                currentCameraMode = CameraModes.FollowingXAndYBottom;
                targetHeight = 0;
            }
        }

        private void SmoothScrollTo(float targetY)
        {
            transform.DOKill();
            float distance = Mathf.Abs(transform.position.y - targetY);
            transform.DOMoveY(targetY, distance / smoothScrollingSpeed);
        }

        private void HardScrollTo(float targetY)
        {
            transform.DOKill();
            var tempPos = new Vector3(transform.position.x,
                targetY,
                cameraOffSet.z);

            transform.position = tempPos;
        }
    }
}