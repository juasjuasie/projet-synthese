﻿using System;
using DG.Tweening;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Gabriel St-Laurent-Recoura
    [Findable(R.S.Tag.CameraShaker)]
    public class CameraShaker : MonoBehaviour
    {
        [SerializeField] private float shakeRange = 100;

        public void ShakeCamera (float shakeForce, float shakeDuration, Vector3 shakePosition)
        {
            var positionX = transform.position.x;
            if(shakePosition.x <= positionX + shakeRange && shakePosition.x >= positionX - shakeRange)
                transform.DOShakePosition(shakeForce, shakeDuration).OnComplete(ResetPosition);
        }

        private void ResetPosition()
        {
            transform.localPosition = Vector3.zero;
        }
    }
}