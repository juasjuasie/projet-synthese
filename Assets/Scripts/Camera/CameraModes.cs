﻿namespace Game
{
    //Author : Gabriel St-Laurent-Recoura
    public enum CameraModes
    {
        FollowingXOnly,
        FollowingXAndYBottom,
        FollowingXAndYTop
    }
}