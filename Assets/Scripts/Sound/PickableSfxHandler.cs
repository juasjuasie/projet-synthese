﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Antony Ménard
    public class PickableSfxHandler : MonoBehaviour
    {
        [SerializeField] private GameObject audioSourcePrefab;
        [SerializeField] private AudioClip pickUpSfx;
        [SerializeField] [Range(0, 1)] private float pickUpBaseVolume = 1;

        private AudioSource pickUpSource;
        private GlobalController globalController;
        private TimeScaleChangedEventChannel timeScaleChangedEventChannel;
        private VolumeChangedEventChannel volumeChangedEventChannel;
        
        private void Awake()
        {
            timeScaleChangedEventChannel = Finder.TimeScaleChangedEventChannel;
            globalController = Finder.GlobalController;
            volumeChangedEventChannel = Finder.VolumeChangedEventChannel;
            SetPickUpSource();
        }

        private void OnEnable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged += OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged += OnVolumeChanged;
        }

        private void OnDisable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged -= OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged -= OnVolumeChanged;
        }
        
        private void OnVolumeChanged(float volume)
        {
            pickUpSource.volume = pickUpBaseVolume * volume;
        }
        
        private void OnTimeScaleChanged(TimeScale timeScale)
        {
            if (timeScale == TimeScale.Stopped || timeScale == TimeScale.Frozen)
                pickUpSource.Pause();
            else
                pickUpSource.UnPause();
        }
        
        private void SetPickUpSource()
        {
            pickUpSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            pickUpSource.clip = pickUpSfx;
            pickUpSource.volume = pickUpBaseVolume * globalController.GetMasterVolume();
        }

        public void PlayPickSfx()
        {
            pickUpSource.Play();
            Destroy(gameObject, pickUpSfx.length);
        }
    }
}