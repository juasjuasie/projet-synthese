﻿using DG.Tweening;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Nicolas Bisson, Antony Ménard
    public class MissileSfxHandler : MonoBehaviour
    {
        [SerializeField] private GameObject audioSourcePrefab;
        [SerializeField] private AudioClip lockOnSfx;
        [SerializeField] [Range(0, 1)] private float lockOnBaseVolume = 1;

        private AudioSource lockOnSource;
        private GlobalController globalController;
        private VolumeChangedEventChannel volumeChangedEventChannel;
        private TimeScaleChangedEventChannel timeScaleChangedEventChannel;
        
        private void Awake()
        {
            timeScaleChangedEventChannel = Finder.TimeScaleChangedEventChannel;
            globalController = Finder.GlobalController;
            volumeChangedEventChannel = Finder.VolumeChangedEventChannel;
            SetLockOnSource();
        }

        private void OnEnable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged += OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged += OnVolumeChanged;
        }

        private void OnDisable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged -= OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged -= OnVolumeChanged;
        }
        
        private void OnVolumeChanged(float volume)
        {
            lockOnSource.volume = lockOnBaseVolume * volume;
        }
        
        private void OnTimeScaleChanged(TimeScale timeScale)
        {
            if (timeScale == TimeScale.Stopped || timeScale == TimeScale.Frozen)
                lockOnSource.Pause();
            else
                lockOnSource.UnPause();
        }
        
        private void SetLockOnSource()
        {
            lockOnSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            lockOnSource.clip = lockOnSfx;
            lockOnSource.volume = lockOnBaseVolume * globalController.GetMasterVolume();
            lockOnSource.loop = true;
        }

        public void PlayLockOnSfx()
        {
            lockOnSource.Play();
        }
    }
}