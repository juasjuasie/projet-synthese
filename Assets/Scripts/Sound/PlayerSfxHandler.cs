﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Nicolas Bisson, Antony Ménard
    public class PlayerSfxHandler : MonoBehaviour
    {
        [SerializeField] private GameObject audioSourcePrefab;
        [SerializeField] private AudioClip jumpSfx;
        [SerializeField] [Range(0, 1)] private float jumpBaseVolume = 1;
        [SerializeField] private AudioClip fireSfx;
        [SerializeField] [Range(0, 1)] private float fireBaseVolume = 1;
        [SerializeField] private AudioClip grindSfx;
        [SerializeField] [Range(0, 1)] private float grindBaseVolume = 1;
        [SerializeField] private AudioClip floorHitSfx;
        [SerializeField] [Range(0, 1)] private float floorHitBaseVolume = 1;
        [SerializeField] private AudioClip deathSfx;
        [SerializeField] [Range(0, 1)] private float deathBaseVolume = 1;
        [SerializeField] private AudioClip gravityChangedSfx;
        [SerializeField] [Range(0, 1)] private float gravityChangedBaseVolume = 1;

        private AudioSource jumpSource;
        private AudioSource fireSource;
        private AudioSource grindSource;
        private AudioSource floorHitSource;
        private AudioSource deathSource;
        private AudioSource gravityChangedSource;
        private GlobalController globalController;
        private VolumeChangedEventChannel volumeChangedEventChannel;
        private TimeScaleChangedEventChannel timeScaleChangedEventChannel;
        
        private void Awake()
        {
            timeScaleChangedEventChannel = Finder.TimeScaleChangedEventChannel;
            globalController = Finder.GlobalController;
            volumeChangedEventChannel = Finder.VolumeChangedEventChannel;
            SetJumpSource();
            SetFireSource();
            SetGrindSource();
            SetFloorHitSource();
            SetDeathSource();
            SetGravityChangedSource();
        }
        
        private void OnEnable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged += OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged += OnVolumeChanged;
        }

        private void OnDisable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged -= OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged -= OnVolumeChanged;
        }

        private void OnTimeScaleChanged(TimeScale timeScale)
        {
            if (timeScale == TimeScale.Stopped || timeScale == TimeScale.Frozen)
                PauseAllSources();
            else
                UnPauseAllSources();
        }

        private void PauseAllSources()
        {
            jumpSource.Pause();
            fireSource.Pause();
            grindSource.Pause();
            floorHitSource.Pause();
            deathSource.Pause();
            gravityChangedSource.Pause();
        }
        
        private void UnPauseAllSources()
        {
            jumpSource.UnPause();
            fireSource.UnPause();
            grindSource.UnPause();
            floorHitSource.UnPause();
            deathSource.UnPause();
            gravityChangedSource.UnPause();
        }
        
        private void OnVolumeChanged(float volume)
        {
            jumpSource.volume = jumpBaseVolume * volume;
            fireSource.volume = fireBaseVolume * volume;
            grindSource.volume = grindBaseVolume * volume;
            floorHitSource.volume = floorHitBaseVolume * volume;
            deathSource.volume = deathBaseVolume * volume;
            gravityChangedSource.volume = gravityChangedBaseVolume * volume;
        }

        private void SetJumpSource()
        {
            jumpSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            jumpSource.clip = jumpSfx;
            jumpSource.volume = jumpBaseVolume * globalController.GetMasterVolume();
        }

        private void SetFireSource()
        {
            fireSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            fireSource.clip = fireSfx;
            fireSource.volume = fireBaseVolume * globalController.GetMasterVolume();
        }

        private void SetGrindSource()
        {
            grindSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            grindSource.clip = grindSfx;
            grindSource.volume = grindBaseVolume * globalController.GetMasterVolume();
        }

        private void SetFloorHitSource()
        {
            floorHitSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            floorHitSource.clip = floorHitSfx;
            floorHitSource.volume = floorHitBaseVolume * globalController.GetMasterVolume();
        }

        private void SetDeathSource()
        {
            deathSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            deathSource.clip = deathSfx;
            deathSource.volume = deathBaseVolume * globalController.GetMasterVolume();
        }

        private void SetGravityChangedSource()
        {
            gravityChangedSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            gravityChangedSource.clip = gravityChangedSfx;
            gravityChangedSource.volume = gravityChangedBaseVolume * globalController.GetMasterVolume();
        }

        public void PlayJumpSfx()
        {
            jumpSource.Play();
        }
        
        public void PlayFireSfx()
        {
            fireSource.Play();
        }
        
        public void PlayGrindSfx()
        {
            if (!grindSource.isPlaying)
                grindSource.Play();
        }
        
        public void StopGrindSfx()
        {
            grindSource.Stop();
        }
        
        public void PlayFloorHitSfx()
        {
            floorHitSource.Play();
        }
        
        public void PlayDeathSfx()
        {
            deathSource.Play();
        }
        
        public void PlayGravityChangedSfx()
        {
            gravityChangedSource.Play();
        }
    }
}