﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Nicolas Bisson, Antony Ménard
    public class JumpPadSfxHandler : MonoBehaviour
    {
        [SerializeField] private GameObject audioSourcePrefab;
        [SerializeField] private AudioClip bounceSfx;
        [SerializeField] [Range(0, 1)] private float bounceBaseVolume = 1;

        private AudioSource bounceSource;
        private GlobalController globalController;
        private TimeScaleChangedEventChannel timeScaleChangedEventChannel;
        private VolumeChangedEventChannel volumeChangedEventChannel;
        
        private void Awake()
        {
            timeScaleChangedEventChannel = Finder.TimeScaleChangedEventChannel;
            globalController = Finder.GlobalController;
            volumeChangedEventChannel = Finder.VolumeChangedEventChannel;
            SetBounceSource();
        }

        private void OnEnable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged += OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged += OnVolumeChanged;
        }

        private void OnDisable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged -= OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged -= OnVolumeChanged;
        }
        
        private void OnVolumeChanged(float volume)
        {
            bounceSource.volume = bounceBaseVolume * volume;
        }
        
        private void OnTimeScaleChanged(TimeScale timeScale)
        {
            if (timeScale == TimeScale.Stopped || timeScale == TimeScale.Frozen)
                bounceSource.Pause();
            else
                bounceSource.UnPause();
        }
        
        private void SetBounceSource()
        {
            bounceSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            bounceSource.clip = bounceSfx;
            bounceSource.volume = bounceBaseVolume * globalController.GetMasterVolume();
        }

        public void PlayBounceSfx()
        {
            bounceSource.Play();
        }
    }
}