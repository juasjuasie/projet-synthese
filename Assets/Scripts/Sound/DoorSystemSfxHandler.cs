﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Nicolas Bisson, Antony Ménard
    public class DoorSystemSfxHandler : MonoBehaviour
    {
        [SerializeField] private GameObject audioSourcePrefab;
        [SerializeField] private AudioClip buttonTriggeredSfx;
        [SerializeField] [Range(0, 1)] private float buttonTriggeredBaseVolume = 1;
        [SerializeField] private AudioClip doorMovingSfx;
        [SerializeField] [Range(0, 1)] private float doorMovingBaseVolume = 1;

        private AudioSource buttonTriggeredSource;
        private AudioSource doorMovingSource;
        private GlobalController globalController;
        private TimeScaleChangedEventChannel timeScaleChangedEventChannel;
        private VolumeChangedEventChannel volumeChangedEventChannel;
        
        private void Awake()
        {
            timeScaleChangedEventChannel = Finder.TimeScaleChangedEventChannel;
            globalController = Finder.GlobalController;
            SetButtonTriggeredSource();
            SetDoorMovingSource();
        }
        
        private void OnEnable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged += OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged += OnVolumeChanged;
        }

        private void OnDisable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged -= OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged -= OnVolumeChanged;
        }

        private void OnVolumeChanged(float volume)
        {
            buttonTriggeredSource.volume = buttonTriggeredBaseVolume * volume;
            doorMovingSource.volume = doorMovingBaseVolume * volume;
        }
        
        private void OnTimeScaleChanged(TimeScale timeScale)
        {
            if (timeScale == TimeScale.Stopped || timeScale == TimeScale.Frozen)
            {
                PauseAllSources();
            }
            else
            {
                UnPauseAllSources();
            }
        }

        private void PauseAllSources()
        {
            buttonTriggeredSource.Pause();
            doorMovingSource.Pause();
        }
        
        private void UnPauseAllSources()
        {
            buttonTriggeredSource.UnPause();
            doorMovingSource.UnPause();
        }
        
        private void SetButtonTriggeredSource()
        {
            volumeChangedEventChannel = Finder.VolumeChangedEventChannel;
            buttonTriggeredSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            buttonTriggeredSource.clip = buttonTriggeredSfx;
            buttonTriggeredSource.volume = buttonTriggeredBaseVolume * globalController.GetMasterVolume();
        }

        private void SetDoorMovingSource()
        {
            doorMovingSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            doorMovingSource.clip = doorMovingSfx;
            doorMovingSource.volume = doorMovingBaseVolume * globalController.GetMasterVolume();
        }

        public void PlayButtonTriggeredSfx()
        {
            buttonTriggeredSource.Play();
        }

        public void PlayDoorMovingSfx()
        {
            doorMovingSource.Play();
        }
    }
}