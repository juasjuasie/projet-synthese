﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Nicolas Bisson, Antony Ménard
    public class EnemySfxHandler : MonoBehaviour
    {
        [SerializeField] private GameObject audioSourcePrefab;
        [SerializeField] private AudioClip fireSfx;
        [SerializeField] [Range(0, 1)] private float fireBaseVolume = 1;
        [SerializeField] private AudioClip deathSfx;
        [SerializeField] [Range(0, 1)] private float deathBaseVolume = 1;

        private AudioSource fireSource;
        private AudioSource deathSource;
        private GlobalController globalController;
        private TimeScaleChangedEventChannel timeScaleChangedEventChannel;
        private VolumeChangedEventChannel volumeChangedEventChannel;

        public AudioSource FireSource => fireSource;

        private void Awake()
        {
            timeScaleChangedEventChannel = Finder.TimeScaleChangedEventChannel;
            globalController = Finder.GlobalController;
            volumeChangedEventChannel = Finder.VolumeChangedEventChannel;
            SetFireSource();
            SetDeathSource();
        }

        private void OnEnable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged += OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged += OnVolumeChanged;
        }

        private void OnDisable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged -= OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged -= OnVolumeChanged;
        }
        
        private void OnVolumeChanged(float volume)
        {
            fireSource.volume = fireBaseVolume * volume;
            deathSource.volume = deathBaseVolume * volume;
        }
        
        private void OnTimeScaleChanged(TimeScale timeScale)
        {
            if (timeScale == TimeScale.Stopped || timeScale == TimeScale.Frozen)
                PauseAllSources();
            else
                UnPauseAllSources();
        }
        
        private void PauseAllSources()
        {
            fireSource.Pause();
            deathSource.Pause();
        }
        
        private void UnPauseAllSources()
        {
            fireSource.UnPause();
            deathSource.UnPause();
        }
        
        private void SetFireSource()
        {
            fireSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            fireSource.clip = fireSfx;
            fireSource.volume = fireBaseVolume * globalController.GetMasterVolume();
        }

        private void SetDeathSource()
        {
            deathSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            deathSource.clip = deathSfx;
            deathSource.volume = deathBaseVolume * globalController.GetMasterVolume();
        }

        public void PlayFireSfx()
        {
            fireSource.Play();
        }

        public void PlayDeathSfx()
        {
            fireSource.Stop();
            deathSource.Play();
            Destroy(gameObject, deathSfx.length);
        }
    }
}