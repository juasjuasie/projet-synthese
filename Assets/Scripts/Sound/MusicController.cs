﻿using System.Collections.Generic;
using DG.Tweening;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Antony Ménard
    [Findable(R.S.Tag.MusicController)]
    public class MusicController : MonoBehaviour
    {
        [SerializeField] private List<AudioClip> audioClipsLevels;

        private const int MENU_MUSIC = 0;
        
        private AudioSource audioSource;
        private TimeScaleChangedEventChannel timeScaleChangedEventChannel;
        private VolumeChangedEventChannel volumeChangedEventChannel;
        private int musicIndex;
        private float lastSavedTime;
        
        private void Awake()
        {
            volumeChangedEventChannel = Finder.VolumeChangedEventChannel;
            lastSavedTime = 0;
            BackToMenuMusic();
            timeScaleChangedEventChannel = Finder.TimeScaleChangedEventChannel;
        }
        
        private void OnEnable()
        {
            volumeChangedEventChannel.OnVolumeChanged += OnVolumeChanged;
            timeScaleChangedEventChannel.OnTimeScaleChanged += OnTimeScaleChanged;
        }

        private void OnDisable()
        {
            volumeChangedEventChannel.OnVolumeChanged -= OnVolumeChanged;
            timeScaleChangedEventChannel.OnTimeScaleChanged -= OnTimeScaleChanged;
        }

        public void NextMusic()
        {
            SetMusicByLevel(musicIndex + 1);
        }
        
        public void BackToMenuMusic()
        {
            if (audioSource == null)
                audioSource = GetComponent<AudioSource>();
            lastSavedTime = 0;
            musicIndex = MENU_MUSIC;
            audioSource.clip = audioClipsLevels[musicIndex];
            audioSource.time = lastSavedTime;
            audioSource.Play();
        }

        private void OnVolumeChanged(float volume)
        {
            audioSource.volume = volume;
        }

        public float GetGameVolume()
        {
            return audioSource.volume;
        }
        
        private void OnTimeScaleChanged(TimeScale newTimeScale)
        {
            if (newTimeScale == TimeScale.Stopped)
            {
                PauseMusic();
            }
            else
            {
                UnpauseMusic();
                audioSource.pitch = Time.timeScale;
            }
        }

        //Author : Nicolas Bisson
        public void ResetToLastSavedTime()
        {
            audioSource.time = lastSavedTime;
        }

        private void PauseMusic()
        {
            audioSource.Pause();
        }

        private void UnpauseMusic()
        {
            audioSource.UnPause();
        }

        public void SaveCurrentMusicTime()
        {
            lastSavedTime = audioSource.time;
        }

        public void SetMusicByLevel(int levelIndex)
        {
            audioSource.Stop();
            musicIndex = levelIndex;
            audioSource.clip = audioClipsLevels[musicIndex];
            lastSavedTime = 0;
            ResetToLastSavedTime();
            audioSource.Play();
        }
    }
}