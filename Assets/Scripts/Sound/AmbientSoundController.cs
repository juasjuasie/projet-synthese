﻿using System;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Antony Ménard
    public class AmbientSoundController : MonoBehaviour
    {
        
        [SerializeField] private GameObject audioSourcePrefab;
        [SerializeField] private AudioClip timeSuspendedClip;
        [SerializeField] [Range(0, 1)] private float timeSuspendedBaseVolume = 1;
        
        private VolumeChangedEventChannel volumeChangedEventChannel;
        private TimeScaleChangedEventChannel timeScaleChangedEventChannel;
        private AudioSource audioSource;
        
        private void Awake()
        {
            volumeChangedEventChannel = Finder.VolumeChangedEventChannel;
            timeScaleChangedEventChannel = Finder.TimeScaleChangedEventChannel;
            SetAmbientSoundSource();
        }

        private void OnEnable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged += OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged += OnVolumeChanged;
        }

        private void OnDisable()
        {
            timeScaleChangedEventChannel.OnTimeScaleChanged -= OnTimeScaleChanged;
            volumeChangedEventChannel.OnVolumeChanged -= OnVolumeChanged;
        }

        private void OnTimeScaleChanged(TimeScale timeScale)
        {
            if (timeScale == TimeScale.Suspended)
            {
                SetClip(timeSuspendedClip);
                PlayAmbientSound();
            }
            else
            {
                StopAmbientSound();
            }
        }

        private void OnVolumeChanged(float volume)
        {
            audioSource.volume = timeSuspendedBaseVolume * volume;
        }

        private void SetClip(AudioClip audioClip)
        {
            audioSource.clip = audioClip;
            audioSource.volume = timeSuspendedBaseVolume;
        }

        private void SetAmbientSoundSource()
        {
            audioSource = Instantiate(audioSourcePrefab, transform).GetComponent<AudioSource>();
            audioSource.loop = true;
            StopAmbientSound();
        }
        
        private void PlayAmbientSound()
        {
            audioSource.Play();
        }
        
        private void StopAmbientSound()
        {
            audioSource.Stop();
        }
    }
}