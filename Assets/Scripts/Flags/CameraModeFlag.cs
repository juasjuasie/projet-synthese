﻿using System;
using Game;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Gabriel St-Laurent-Recoura
    public class CameraModeFlag : Flag
    {
        [SerializeField] private CameraModes cameraMode;
        [SerializeField] private float targetHeight = 0;
        [SerializeField] private float SmoothScrollingSpeed = 5f;
        
        private CameraMover cameraMover;

        protected void Awake()
        {
            base.Awake();
            cameraMover = Finder.CameraMover;
        }

        protected override void OnPlayerSensed(PlayerController player)
        {
            cameraMover.ChangeCameraMode(cameraMode, targetHeight, SmoothScrollingSpeed);
        }
    }
}