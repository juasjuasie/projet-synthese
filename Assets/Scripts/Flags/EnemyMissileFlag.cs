﻿

using UnityEngine;

namespace Game
{
    //Author : Antony Ménard
    public class EnemyMissileFlag : Flag
    {
        private new void Awake()
        {
            base.Awake();
        }
        
        protected override void OnPlayerSensed(PlayerController player)
        {
            transform.parent.GetComponentInParent<MissileEnemy>().ShootMissile(); 
        }
    }
}