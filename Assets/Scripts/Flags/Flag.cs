﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Gabriel St-Laurent-Recoura
    public abstract class Flag : MonoBehaviour
    {
        protected ISensor<PlayerController> sensorPlayer;

        protected void Awake()
        {
            sensorPlayer = GetComponentInChildren<Sensor>().For<PlayerController>();
        }
        
        protected void OnEnable()
        {
            sensorPlayer.OnSensedObject += OnPlayerSensed;
        }
        
        protected void OnDisable()
        {
            sensorPlayer.OnSensedObject -= OnPlayerSensed;
        }

        protected abstract void OnPlayerSensed(PlayerController player);
    }
}