﻿using System.Collections;
using System.Collections.Generic;
using Harmony;
using UnityEngine;

namespace Game
{
    //Author : Gabriel St-Laurent-Recoura
    public class CheckpointFlag : Flag
    {
        [SerializeField] private GameObject playerSpawnPoint;
        [SerializeField] [Range(1, 100)] private int secondsToShowCanvas = 2;
        [SerializeField] private List<ParticleController> particleControllers;
        
        private CheckpointCanvasController checkPointCanvas;
        private GameController gameController;
      
        protected void Awake()
        {
            base.Awake();
            checkPointCanvas = Finder.CheckpointCanvasController;
            gameController = Finder.GameController;
        }

        protected override void OnPlayerSensed(PlayerController player)
        {
            StartCoroutine(ActivateCheckpointCanvas());
            gameController.SetCheckpoint(playerSpawnPoint.transform.position);
        }

        private IEnumerator ActivateCheckpointCanvas()
        {
            ActivateParticles(true);
            checkPointCanvas.ActivateCanvas(true);
            yield return new WaitForSeconds(secondsToShowCanvas);
            checkPointCanvas.ActivateCanvas(false);
            ActivateParticles(false);
        }

        private void ActivateParticles(bool areParticlesNeeded)
        {
            foreach (var particleController in particleControllers)
            {
                particleController.ActivateParticles(areParticlesNeeded);
            }
        }
    }
}