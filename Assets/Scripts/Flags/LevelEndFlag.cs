﻿using Harmony;
using UnityEngine;

namespace Game
{
    //Author: Gabriel St-Laurent-Recoura
    public class LevelEndFlag : Flag
    {
        private GameController gameController;

        private void Awake()
        {
            base.Awake();
            gameController = Finder.GameController;
        }

        protected override void OnPlayerSensed(PlayerController player)
        {
           gameController.NextLevel();
        }
    }
}