﻿using System;
using UnityEngine;

namespace Game
{
    //Author: Antony Ménard, Gabriel St-Laurent-Recoura
    public class KillFlag : Flag
    {
        protected override void OnPlayerSensed(PlayerController player)
        {
            player.Kill();
        }
    }
}